<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arResult['CHAPTER_STEPS']): ?>
    <div class="chapters" data-type="<?= $arResult['PROPERTIES']['COLOR']['VALUE'] ?>" data-view="marks">
        <div class="chapters-info">
            <div class="chapters-info__container swiper-container chapters-slider">
                <div class="chapters-info__inner swiper-wrapper">
                    <? $index = 0; ?>
                    <? foreach ($arResult['CHAPTER_STEPS'] as $key => $step): ?>
                        <div class="chapters-info__item swiper-slide">
                            <? if ($index != 0): ?>
                                <div class="chapters-info__label">
                                    <h4><?= $step['NAME'] ?></h4>
                                </div>
                            <? endif; ?>
                            <div class="chapters-info__basis">
                                <? if ($step['IMAGE']): ?>
                                    <div class="chapters-info__illustration">
                                        <picture>
                                            <? if ($step['IMAGE_MOB']): ?>
                                                <source media="(max-width:767px)"
                                                        data-srcset="<?= $step['IMAGE_MOB'] ?>">
                                            <? endif; ?>
                                            <img class="lazy" data-src="<?= $step['IMAGE'] ?>"
                                                 alt="<?= $step['NAME'] ?>">
                                        </picture>
                                    </div>
                                <? endif; ?>
                                <div class="chapters-info__line">
                                    <? if ($step['ADD_TEXT']): ?>
                                        <h1><?= $step['ADD_TEXT']['~VALUE']['TEXT'] ?></h1>
                                    <? endif; ?>
                                    <? if ($step['ADD_TEXT2']): ?>
                                        <div class="heading heading_level-4"><?= $step['ADD_TEXT2']['~VALUE']['TEXT'] ?></div>
                                    <? endif; ?>
                                    <? if ($step['PREVIEW_TEXT']): ?>
                                        <div class="heading heading_level-3"><?= $step['PREVIEW_TEXT']['~VALUE']['TEXT'] ?></div>
                                    <? endif; ?>
                                </div>
                                <? if ($step['INFO']): ?>
                                    <div class="chapters-info__line">
                                        <? if (is_array($step['INFO']['VALUE'])): ?>
                                            <div class="chapters-info__content">
                                                <div class="chapters-info__content-row">
                                                    <? foreach ($step['INFO']['VALUE'] as $key => $info): ?>
                                                        <? if ($step['INFO']['DESCRIPTION'][$key] != ''): ?>
                                                            <div class="chapters-info__content-column">
                                                                <div class="chapters-info__content-item">
                                                                    <span class="chapters-info__content-count"><?= $info ?></span>
                                                                    <div class="chapters-info__content-desc"><?= $step['INFO']['DESCRIPTION'][$key] ?></div>
                                                                </div>
                                                            </div>
                                                        <? else: ?>
                                                            <div class="chapters-info__advantages-column">
                                                                <div class="chapters-info__advantages-item">
                                                                    <div class="chapters-info__advantages-desc"><?= $info ?></div>
                                                                </div>
                                                            </div>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                <? endif; ?>
                                <? if (!is_array($step['INFO']['VALUE']) && isset($step['INFO']['VALUE'])): ?>
                                    <a class="chapters-info__more" href="<?= $step['INFO']['VALUE'] ?>">
                                            <span class="chapters-info__more-icon">
                                                <svg class="arrow" width="14" height="14">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                                </svg>
                                            </span>
                                        <div class="chapters-info__more-line">
                                            <div class="chapters-info__more-brand">
                                                <?= file_get_contents($step['IMAGE_SITE']) ?>
                                            </div>
                                            <h4><?= GetMessage('MOVE_TO_SITE') ?></h4>
                                        </div>
                                    </a>
                                <? endif; ?>
                            </div>
                        </div>
                        <? $index++; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
        <div class="chapters-steps">
            <? foreach ($arResult['CHAPTER_STEPS'] as $key => $step): ?>
                <div class="chapters-steps__item <?= ($key == 0 ? 'chapters-steps__item_active' : false) ?>"
                     data-index="<?= $key ?>">
                    <div class="chapters-steps__item-column">
                        <div class="chapters-steps__item-line">
                            <div class="chapters-steps__item-icon">
                                <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . $step['ICO']) ?>
                            </div>
                        </div>
                        <div class="chapters-steps__item-line">
                            <h4><?= $step['NAME'] ?></h4>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif; ?>