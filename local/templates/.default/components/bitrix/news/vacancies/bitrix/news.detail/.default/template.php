<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="information information_interior">
    <? $pageInfo = IL\Catalog::getElementList(ILBOCK_PAGES_ID, ['ID' => $arParams['ADD_PAGE_INFO']]); ?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/header/inner_bg_column.php',
        ['ELEMENT' => $pageInfo[0], 'OTHER_NAME' => $arResult['NAME']], ['SHOW_BORDER' => false]) ?>
    <div class="information-scroll">
        <div class="information-scroll__inner">
            <div class="information-content">
                <a class="back-link" href="<?= $arParams['IBLOCK_URL'] ?>">
                    <div class="back-link__icon">
                        <svg class="icon__back-link" width="15" height="15">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#back-link"></use>
                        </svg>
                    </div>
                    <span class="back-link__label"><?= GetMessage('MOVE_BACK') ?></span>
                </a>
                <div class="max-width">
                    <?= $arResult['PREVIEW_TEXT'] ?>
                    <? if ($arResult['PROPERTIES']['CHARGE']['~VALUE']['TEXT']): ?>
                        <h6><?= GetMessage('CHARGE') ?></h6>
                        <?= $arResult['PROPERTIES']['CHARGE']['~VALUE']['TEXT']; ?>
                    <? endif; ?>
                    <? if ($arResult['PROPERTIES']['REQUIREMENTS']['~VALUE']['TEXT']): ?>
                        <h6><?= GetMessage('REQUIREMENTS') ?></h6>
                        <?= $arResult['PROPERTIES']['REQUIREMENTS']['~VALUE']['TEXT']; ?>
                    <? endif; ?>
                    <? if ($arResult['PROPERTIES']['CONDITIONS']['~VALUE']['TEXT']): ?>
                        <h6><?= GetMessage('CONDITIONS') ?></h6>
                        <?= $arResult['PROPERTIES']['CONDITIONS']['~VALUE']['TEXT']; ?>
                    <? endif; ?>
                </div>
                <a class="btn-next js__modal-btn" href="#" data-toggle="modal" data-target="#modalVacancies"
                   data-title="<?= $arResult['NAME'] ?>">
                    <span class="btn-next__label"><?= GetMessage('RESPOND') ?></span>
                    <div class="btn-next__icon">
                        <svg class="icon__arrow-up-right" width="14" height="14">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                        </svg>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>