<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

if (CModule::IncludeModule("iblock")) {

    $IBLOCK_ID = ILBOCK_MARKS_ID;

    $arOrder = ["SORT" => "DESC"];
    $arSelect = ["ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_LOGO", "PROPERTY_NAME_TWO"];
    $arFilter = ["IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y"];
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $aMenuLinksExt[] = [
            $arFields['NAME'],
            $arFields['DETAIL_PAGE_URL'],
            [],
            [
                "LOGO" => $arFields['PROPERTY_LOGO_VALUE'],
                "NAME_TWO" => $arFields['PROPERTY_NAME_TWO_VALUE'],
            ],
            "",
        ];
    }
}

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);