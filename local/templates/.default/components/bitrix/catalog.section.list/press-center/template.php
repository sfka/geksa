<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = ["CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')]; ?>
<? $pageInfo = IL\Catalog::getElementList(ILBOCK_PAGES_ID, ['ID' => $arParams['ADD_PAGE_INFO']]); ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/header/inner_bg.php', ['ELEMENT' => $pageInfo[0]], ['SHOW_BORDER' => false]) ?>
<div id="target-container">
	<? foreach ($arResult['SECTIONS'] as &$arSection): ?>
		<?php $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams); ?>
		<div class="section section_padding news" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
			<div class="section-wrap">
				<div class="heading heading_level-1 heading_color heading_medium heading_max-width"><?= ($arSection['UF_ADD_NAME'] ? $arSection['UF_ADD_NAME'] : $arSection['NAME']) ?></div>
				<div class="news-list">
					<? foreach ($arSection['ITEMS'] as $arItem): ?>
						<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
						?>
						<a class="news-item" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
						   id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
							<div class="content">
								<div class="news-item__wrap">
									<div class="news-img">
										<div class="news-img__bg">
											<img src="<?= CFile::GetPath($arItem['PREVIEW_PICTURE']) ?>"
												 alt="<?= $arItem['NAME'] ?>">
										</div>
										<span class="news-img__more">
												<svg class="icon__arrow-up-right" width="14" height="14">
													<use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
												</svg>
											</span>
									</div>
									<div class="news-content">
										<span><?= $arItem['ACTIVE_FROM'] ?></span>
										<h4><?= $arItem['NAME'] ?></h4>
										<p><?= $arItem['PREVIEW_TEXT'] ?></p>
									</div>
								</div>
							</div>
						</a>
					<? endforeach; ?></div>
				<div class="news-more">
					<div class="content">
						<a class="btn-next" href="<?= $arSection['SECTION_PAGE_URL'] ?>">
							<span class="btn-next__label"><?= ($arSection['UF_SHOW_MORE'] ? $arSection['UF_SHOW_MORE'] : GetMessage('SHOW_MORE')) ?></span>
							<div class="btn-next__icon">
								<svg class="arrow" width="15" height="15">
									<use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
								</svg>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	<? endforeach; ?>
</div>