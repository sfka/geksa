<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="megamenu__list-inner">
        <? foreach ($arResult as $arItem): ?>
            <? if ($arItem['IS_PARENT']): ?>
                <div class="megamenu__list-item megamenu__list-item_accordion">
                    <a class="megamenu__list-link" href="<?= $arItem['LINK'] ?>"><?= $arItem['TEXT'] ?>
                        <svg class="icon__nav-menu_arrow" width="7" height="4">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#nav-menu_arrow"></use>
                        </svg>
                    </a>
                    <ul class="megamenu__sublist">
                        <? foreach ($arItem['ITEMS'] as $arSubItem): ?>
                            <li class="megamenu__sublist-item">
                                <a class="megamenu__sublist-link"
                                   href="<?= $arSubItem['LINK'] ?>"><?= $arSubItem['TEXT'] ?></a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            <? endif; ?>
        <? endforeach; ?>
        <? if ($arResult['NO_PARENT']): ?>
            <div class="megamenu__list-item">
                <? foreach ($arResult['NO_PARENT'] as $arParItem): ?>
                    <a class="megamenu__list-link" href="<?= $arParItem['LINK'] ?>"><?= $arParItem['TEXT'] ?></a>
                <? endforeach; ?>
            </div>
        <? endif; ?>
    </div>
<? endif; ?>
