<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="footer__row footer__row_base">
    <div class="footer__item">
        <div class="footer__copyright"><?= GetMessage('COPYRIGHT') ?></div>
    </div>
    <? if (!empty($arResult)): ?>
        <?
        foreach ($arResult as $arItem):
            if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
            ?>
            <div class="footer__item">
                <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
            </div>
        <? endforeach ?>
    <? endif ?>
</div>