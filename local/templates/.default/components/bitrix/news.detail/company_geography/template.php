<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section section_padding geography" id="target-container">
    <div class="section-wrap section-wrap_center">
        <div class="heading heading_level-1 heading_medium heading_color heading_max-width"><?= $arResult['PROPERTIES']['NAME_GEO']['VALUE'] ?></div>
        <div class="geography-wrap">
            <div class="geography-info">
                <div class="geography-map">
                    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/company_geography.php', [
                        'IBLOCK_ID' => ILBOCK_CITIES_ID,
                        'MAP' => CFile::GetPath($arResult['PROPERTIES']['BG_GEO']['VALUE']),
                        'NAME' => $arResult['PROPERTIES']['NAME_GEO']['VALUE'],
                    ], ['SHOW_BORDER' => false]) ?>
                </div>
            </div>
            <div class="geography-counts">
                <? $index = 0; ?>
                <? foreach ($arResult['PROPERTIES']['NUMBERS_GEO']['VALUE'] as $key => $value): ?>
                    <? $index++; ?>
                    <? $type = 1; ?>
                    <? if ($index % 2 == 0) {
                        $type = 2;
                    } elseif ($index > 2) {
                        $type = 3;
                    } ?>
                    <div class="geography-counts__item geography-counts__item_type-<?= $type ?> <?= ($type == 2 ? 'rellax' : false) ?>"
                        <?= ($type == 2 ? 'data-rellax-speed="1.7"' : false) ?>>
                        <span class="geography-counts__item-count count"><?= $value ?>
                            <? $numberValue = preg_replace('~\D+~', '', $value); ?>
                            <span class="count-inner">
                                <? for ($i = $numberValue - 3; $i <= $numberValue; $i++) { ?>
                                    <span><?= $i ?><?=preg_replace('/\d/', '', $value); ?></span>
                                <? } ?>
                            </span>
                        </span>
                        <h4><?= $arResult['PROPERTIES']['NUMBERS_GEO']["DESCRIPTION"][$key] ?></h4>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>