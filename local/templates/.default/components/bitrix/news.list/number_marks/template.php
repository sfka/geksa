<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section section_padding numbers" data-type="blue">
    <div class="section-wrap">
        <div class="heading heading_level-1 heading_medium heading_color"><?= GetMessage('TRADE_MARK_NUMBERS') ?></div>
        <div class="numbers-wrap" data-sticky-container>
            <div class="numbers-counts sticky" data-sticky-class="is-sticky" data-sticky-wrap="true">
                <div class="numbers-counts__land">
                    <? foreach ($arResult['ITEMS'] as $arItem): ?>
                        <div class="numbers-counts__land-item"
                             data-color="<?= $arItem['PROPERTIES']['COLOR']['VALUE'] ?>">
                            <img src="<?= CFile::GetPath($arItem['PROPERTIES']['SVG_MAN_ABOUT']['VALUE']) ?>" alt="">
                        </div>
                    <? endforeach; ?>
                </div>
                <div class="numbers-counts__count">
                    <div class="numbers-counts__count-inner">
                        <? foreach ($arResult['ITEMS'] as $arItem): ?>
                            <div class="numbers-counts__count-item"><?= $arItem['PROPERTIES']['COMPANY_NUMBER']['VALUE'] ?></div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="numbers-info">
                <div class="numbers-list">
                    <div class="numbers-list__scroll">
                        <? $index = 0; ?>
                        <? foreach ($arResult['ITEMS'] as $arItem): ?>
                            <a class="numbers-list__item <?= ($index == 0 ? 'numbers-list__item_active' : false) ?>"
                               href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                               data-color="<?= $arItem['PROPERTIES']['COLOR']['VALUE'] ?>"
                               data-count="<?= $arItem['PROPERTIES']['COMPANY_NUMBER']['VALUE'] ?>">
                                <div class="numbers-list__item-title"><?= $arItem['PROPERTIES']['COMPANY_NUMBER']['VALUE'] . ' ' . $arItem['PROPERTIES']['COMPANY_NUMBER']['DESCRIPTION'] ?></div>
                                <div class="numbers-list__item-info">
                                    <div class="numbers-list__item-brand">
                                        <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arItem['PROPERTIES']['LOGO_BLACK']['VALUE'])) ?>
                                    </div>
                                    <span class="numbers-list__item-icon">
                                    <svg class="arrow" width="14" height="14">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                    </svg>
                                </span>
                                </div>
                            </a>
                            <? $index++; ?>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>