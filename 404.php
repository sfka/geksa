<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");
?>
<div class="container">
    <div class="not-found">
        <div class="content">
            <div class="not-found__line">
                <div class="not-found__center">
                    <div class="not-found__figure">
                        <img src="<?= SITE_STYLE_PATH ?>/img/content/not-found/404.svg" alt="">
                    </div>
                    <p><?= GetMessage('ERROR_404') ?></p>
                </div>
            </div>
            <div class="not-found__link">
                <a class="btn-next" href="<?= SITE_DIR ?>">
                    <span class="btn-next__label"><?= GetMessage('MOTE_TO_MAIN') ?></span>
                    <div class="btn-next__icon">
                        <svg class="arrow" width="15" height="15">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                        </svg>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
