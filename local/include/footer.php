<footer class="footer <?= ($_SERVER['REAL_FILE_PATH'] == '/marks.php' || CSite::InDir('/index.php') ? 'footer_hide' : false) ?>">
    <div class="footer-top">
        <div class="footer__inner">
            <div class="footer__wrap">
                <a class="footer__logo-link" href="<?= SITE_DIR ?>">
                    <div class="footer__logo">
                        <img src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_LOGO_FOOTER")); ?>"
                             alt=""/>
                    </div>
                </a>
                <div class="footer__row footer__row_base">
                    <div class="footer__item">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "marks_menu",
                            [
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => [
                                ],
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "marks",
                                "USE_EXT" => "Y",
                                "COMPONENT_TEMPLATE" => "marks_menu",
                            ],
                            $component,
                            ['HIDE_ICONS' => 'Y']
                        ); ?>
                    </div>
                    <div class="footer__item">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "footermenu_body",
                            [
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "2",
                                "MENU_CACHE_GET_VARS" => [
                                ],
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "bottom_main",
                                "USE_EXT" => "N",
                                "COMPONENT_TEMPLATE" => "footermenu_body",
                            ],
                            false
                        ); ?>
                    </div>
                    <div class="footer__item">
                        <div class="footer__socials">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "footer_social",
                                [
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "N",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "A",
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "N",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "FIELD_CODE" => [
                                        0 => "",
                                        1 => "",
                                    ],
                                    "FILTER_NAME" => "",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "IBLOCK_ID" => "4",
                                    "IBLOCK_TYPE" => "widgets",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "MESSAGE_404" => "",
                                    "NEWS_COUNT" => "20",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => ".default",
                                    "PAGER_TITLE" => "Новости",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "PROPERTY_CODE" => [
                                        0 => "LINK",
                                        1 => "",
                                    ],
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_LAST_MODIFIED" => "N",
                                    "SET_META_DESCRIPTION" => "N",
                                    "SET_META_KEYWORDS" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "N",
                                    "SHOW_404" => "N",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_ORDER2" => "ASC",
                                    "STRICT_SECTION_CHECK" => "N",
                                    "COMPONENT_TEMPLATE" => "footer_social",
                                ],
                                false
                            ); ?>
                        </div>
                        <div class="footer__info">
                            <div class="footer__info-item">
                                <? $phone = \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE") ?>
                                <a href="tel:<?= preg_replace('/[^\d+]+/', '', $phone) ?>"><?= $phone; ?></a>
                            </div>
                            <div class="footer__info-item"><?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_ADDRESS"); ?></div>
                        </div>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "footermenu_bottom",
                            [
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => [
                                ],
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "bottom_footer",
                                "USE_EXT" => "N",
                                "COMPONENT_TEMPLATE" => "footermenu_bottom",
                            ],
                            false
                        ); ?>
                        <div class="footer__creator">
                            <div class="creator">
                                <div class="creator-text"><?= GetMessage('DEVELOPMENT') ?></div>
                                <a class="creator-logo" href="https://ilartech.com/" target="_blank">
                                    <img src="<?= SITE_STYLE_PATH ?>/img/content/creator/creator.svg" alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="footer__inner">
            <div class="footer__wrap">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footermenu_bottom_copy",
                    [
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => [
                        ],
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "bottom_footer_copy",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "footermenu_bottom_copy",
                    ],
                    false
                ); ?>
            </div>
        </div>
    </div>
</footer>