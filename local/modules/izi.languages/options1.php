<?

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\HttpApplication,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialcharsbx($request['mid']) != '' ? $request['mid'] : $request['id'];
Loader::includeModule($module_id);
$rsSites = CSite::GetList($by = "sort", $order = "asc");
while ($arSite = $rsSites->Fetch()) {
    $siteList[] = $arSite;
    $aTabs[] = [
        'DIV' => 'edit_' . $arSite['LID'],
        'SITE' => $arSite['LID'],
        'TAB' => $arSite['NAME'],
        'TITLE' => 'Файл перевода на ' . $arSite['NAME'],
        'OPTIONS' => [
            'TITLE' => Loc::getMessage("IZI_LANG_OPTIONS_FILE"),
            'OPTIONS' => [
                "file_" . $arSite['LID'],
                Loc::getMessage("IZI_LANG_OPTIONS_FILE_LOAD"),
                "",
                ["text", "80"],
            ],
        ],
    ];
}
if ($request->isPost() && check_bitrix_sessid()) {
    echo('asdasdasd');
    foreach ($aTabs as $aTab) {
        foreach ($aTab["OPTIONS"] as $arOption) {
            if (!is_array($arOption)) {
                continue;
            }
            if ($arOption["note"]) {
                continue;
            }
            if ($request["apply"]) {
                $optionValue = $request->getPost($arOption[0]);
                if ($arOption[0] == "switch_on") {
                    if ($optionValue == "") {
                        $optionValue = "N";
                    }
                }
                Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
            } elseif ($request["default"]) {
                Option::set($module_id, $arOption[0], $arOption[2]);
            }
        }
    }
    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . $module_id . "&lang=" . LANG);
}
$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs
);

$tabControl->Begin(); ?>
    <form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>"
          method="post">

        <?
        foreach ($aTabs as $aTab) {
            if ($aTab["OPTIONS"]) {

                $tabControl->BeginNextTab();

                __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
            }
            $fileInput = COption::GetOptionString($module_id, "file_" . $aTab['SITE']);
            if (!empty($fileInput)) {?>
                <tr>
                    <td></td>
                    <td>
                        <a class="adm-btn" href="javascript: new BX.CAdminDialog({'content_url':'/bitrix/admin/public_file_edit.php?site=s1&amp;bxpublic=Y&amp;from=includefile&amp;noeditor=Y&amp;templateID=12&amp;path=<?=$fileInput ?>&amp;lang=ru&amp;template=&amp;subdialog=Y&amp;siteTemplateId=12','width':'1009','height':'503'}).Show();" name="LICENCE_TEXT_s1" title="Редактировать">Редактировать</a>
                    </td>
                </tr>
            <?
            }
        }

        $tabControl->Buttons();
        ?>

        <input type="submit" name="apply" value="<? echo(Loc::GetMessage("IZI_LANG_OPTIONS_INPUT_APPLY")); ?>"
               class="adm-btn-save"/>
        <input type="submit" name="default" value="<? echo(Loc::GetMessage("IZI_LANG_OPTIONS_INPUT_DEFAULT")); ?>"/>

        <?
        echo(bitrix_sessid_post());
        ?>

    </form>
<? $tabControl->End(); ?>