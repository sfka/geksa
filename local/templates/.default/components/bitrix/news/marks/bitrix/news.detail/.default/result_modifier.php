<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arResult['CHAPTER_STEPS'] = [
    [
        'NAME' => $arResult['NAME'],
        'ICO' => SITE_STYLE_PATH . '/img/content/brands/icons/icon-1.svg',
        'ADD_TEXT' => $arResult['PROPERTIES']['ADD_NAME'],
        'ADD_TEXT2' => $arResult['PROPERTIES']['ADD_NAME_TEXT'],
        'IMAGE' => CFile::GetPath($arResult['PROPERTIES']['ADD_NAME_IMAGE']['VALUE']),
    ],
    [
        'NAME' => GetMessage('MARK_NUMBERS'),
        'ICO' => SITE_STYLE_PATH . '/img/content/brands/icons/icon-2.svg',
        'INFO' => $arResult['PROPERTIES']['NUMBERS'],
        'PREVIEW_TEXT' => $arResult['PROPERTIES']['NUMBERS_TEXT'],
        'IMAGE' => CFile::GetPath($arResult['PROPERTIES']['NUMBERS_IMAGE']['VALUE']),
    ],
    [
        'NAME' => GetMessage('MARK_PRODUCTS'),
        'ICO' => SITE_STYLE_PATH . '/img/content/brands/icons/icon-3.svg',
        'INFO' => $arResult['PROPERTIES']['PRODUCTS'],
        'PREVIEW_TEXT' => $arResult['PROPERTIES']['PRODUCTS_TEXT'],
        'IMAGE' => CFile::GetPath($arResult['PROPERTIES']['PRODUCTS_IMAGE']['VALUE']),
    ],
    [
        'NAME' => GetMessage('MARK_CONTACTS'),
        'ICO' => SITE_STYLE_PATH . '/img/content/brands/icons/icon-4.svg',
        'INFO' => $arResult['PROPERTIES']['LINK'],
        'PREVIEW_TEXT' => $arResult['PROPERTIES']['LINK_TEXT'],
        'IMAGE' => CFile::GetPath($arResult['PROPERTIES']['LINK_IMAGE']['VALUE']),
        'IMAGE_SITE' => CFile::GetPath($arResult['PROPERTIES']['LINK_IMAGE_SITE']['VALUE']),
    ],
];