$(document).ready(function () {
    initForms();
    initAjaxPagination();
    updateModalTitle();
    updateModalBody();
    inputCity();
    setCity();
});

function initForms() {
    var body = $('body');
    body.on('submit', '.ajax__form', function (event) {
        event.preventDefault();
        var elem = $(this);
        var url = elem.attr('action');
        var email = elem.find('input[type=email]').val();
        if (email === undefined) {
            email = 'test@test.ru';
        }
        var data = elem.serialize();
        if (elem.hasClass('form__file')) {
            data = new FormData(elem[0]);
            $.ajax({
                type: "POST",
                cache: false,
                processData: false,
                contentType: false,
                url: url,
                data: data,
                dataType: "json",
                beforeSend: function () {
                },
                success: function (res) {
                    if (res['status'] === 'ok') {
                        elem.trigger('reset');
                        elem.closest('.modal').addClass('is-success');
                        formValidationSuccess();
                        //$('.modal-close').trigger('click');
                    }
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                beforeSend: function () {
                },
                success: function (res) {
                    if (res['status'] === 'ok') {
                        elem.trigger('reset');
                        elem.closest('.modal').addClass('is-success');
                        formValidationSuccess();
                        //$('.modal-close').trigger('click');
                    }
                }
            });
        }
    });
}

function initAjaxPagination() {
    $('body').on('click', '.ajax__more-link', function (event) {
        event.preventDefault();
        var elem = $(this);
        var page = parseInt(elem.attr('data-page')) + 1;
        var pagen = elem.attr('data-pagen');
        var allPage = parseInt(elem.attr('data-all-page'));
        if (page == allPage) {
            elem.addClass('hidden');
        }

        if (page <= allPage) {
            var blockID = '.ajax__more-content';
            var url = window.location.href.split('?')[0]
            $.ajax({
                type: "GET",
                url: url + '?PAGEN_' + pagen + '=' + page,
                beforeSend: function () {
                    $(blockID).fadeTo(200, 0.1);
                    $('html, body').animate({
                        scrollTop: $(blockID).offset().top + $(blockID).prop("scrollHeight")
                    }, 700);
                },
                success: function (data) {
                    elem.attr('data-page', page);
                    var $text = $(data).find('.ajax__more-content').html();
                    $(blockID).append($text);
                    $(blockID).fadeTo(200, 1);
                },
            });
        }
    });
}

function updateModalTitle() {
    $('body').on('click', '.js__modal-btn', function () {
        var title = $(this).attr('data-title');
        var target = $(this).attr('data-target');
        $(target).find('.js__modal-title').text(title);
        $(target).find('#vacancy_id').val(title);
    });
}

function updateModalBody() {
    $('body').on('click', '.ajax__modal-update', function (event) {
        event.preventDefault();
        var elementID = $(this).attr('data-element');
        var blockID = $('.ajax__modal-content');
        var data = {};

        data['ELEMENT'] = elementID;

        $.ajax({
            type: "GET",
            url: window.location.href,
            data: data,
            beforeSend: function () {
            },
            success: function (data) {
                blockID.html($(data).find('.ajax__content'));
            }
        });
    })
}

function inputCity() {
    $('body').on('input', '.js__input_city', function () {
        var value = $(this).val().toLowerCase();
        $('li[data-city]').each(function () {
            var city = $(this).attr('data-city').toLowerCase();
            if (city.indexOf(value) == 0) {
                $(this).css('display', 'block');
            } else {
                $(this).css('display', 'none');
            }
        })
    });
}

function setCity() {
    $('body').on('click', '.ajax__choose_city', function () {
        var city = $(this).attr('data-id');
        var url = '/local/ajax/set_session_city.php';
        var data = {
            city: city
        }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status === 'ok') {
                    window.location.reload(true);
                }
            }
        });
    });
}