<? /** @var $block array */ ?><?

/*
$preview = Sprint\Editor\Blocks\Image::getImage($block['preview'], array(
    'width' => 1024,
    'height' => 768,
    'exact' => 0,
    //'jpg_quality' => 75
));

<img alt="<?= $preview['DESCRIPTION'] ?>" src="<?= $preview['SRC'] ?>">

*/

?>
<figure>
    <div class="video">
        <iframe data-type="iframe" width="560" height="315" src="<?= $block['url'] ?>" frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
        <div class="video-play">
            <span class="video-play__icon"></span>
            <img class="dynamic" src="<?= $block['preview']['file']['SRC'] ?>" alt="<?= $block['preview']['desc'] ?>">
        </div>
    </div>
    <figcaption><?= $block['preview']['desc'] ?></figcaption>
</figure>