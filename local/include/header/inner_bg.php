<div class="cover cover_interior">
    <? if ($arParams['ELEMENT']['PROPERTIES']['VIDEO_MAIN']['VALUE']): ?>
        <div class="cover__bg">
            <video autoplay="true" loop="true" muted="true" pip="false"
                   poster="<?= $arParams['ELEMENT']['PREVIEW_PICTURE'] ?>" playsinline>
                <source src="<?= $arParams['ELEMENT']['PROPERTIES']['VIDEO_MAIN']['VALUE']['path'] ?>" type="video/mp4">
            </video>
        </div>
    <? else: ?>
        <div class="cover__bg"
             style="background-image: url(<?= CFile::GetPath($arParams['ELEMENT']['PREVIEW_PICTURE']); ?>)"></div>
    <? endif; ?>
    <div class="cover-wrap">
        <div class="cover-content">
            <div class="cover-row">
                <div class="cover-column">
                    <h1><?= $arParams['ELEMENT']['NAME'] ?></h1>
                    <p><?= $arParams['ELEMENT']['PREVIEW_TEXT'] ?></p>
                </div>
                <div class="cover-column">
                    <div class="scroll-bottom" data-href="#target-container">
                        <span class="scroll-bottom__label"><?= GetMessage('MOVE_DOWN') ?></span>
                        <div class="scroll-bottom__icon">
                            <svg class="arrow" width="20" height="20">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-bottom"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>