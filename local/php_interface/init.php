<?
require $_SERVER["DOCUMENT_ROOT"] . "/local/vendor/autoload.php";

define("SITE_STYLE_PATH", "/local/styles");
define("SITE_INCLUDE_PATH", "/local/include");
define("SITE_USER_CLASS_PATH", "/local/php_interface/user_class");
define("SITE_AJAX_PATH", "/local/ajax");

define("ILBOCK_MARKS_ID", 1);
define("ILBOCK_CERTIFICATES_ID", 2);
define("ILBOCK_PAGES_ID", 3);
define("ILBOCK_CITIES_ID", 8);
define("IBLOCK_FORM_VACANCY", 9);
define("IBLOCK_FORM_CONTACTS", 18);
define("IBLOCK_FORM_COOPERATION", 19);

AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error()
{
    if (defined('ERROR_404') && ERROR_404 == 'Y' || CHTTP::GetLastStatus() == "404 Not Found") {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        require $_SERVER['DOCUMENT_ROOT'] . '/local/templates/inner/header.php';
        require $_SERVER['DOCUMENT_ROOT'] . '/404.php';
        require $_SERVER['DOCUMENT_ROOT'] . '/local/templates/inner/footer.php';
    }
}
