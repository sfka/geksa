<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section section_padding news">
    <div class="section-wrap">
        <div class="heading heading_level-1 heading_color heading_medium heading_max-width"><?= $arResult['SECTION']['PATH'][0]['NAME'] ?></div>
        <div class="news-list ajax__more-content">
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                ?>
                <a class="news-item" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                   id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <div class="content">
                        <div class="news-item__wrap">
                            <div class="news-img">
                                <div class="news-img__bg">
                                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                                         alt="<?= $arItem['NAME'] ?>">
                                </div>
                                <span class="news-img__more">
                                <svg class="icon__arrow-up-right" width="14" height="14">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                </svg>
                    </span>
                            </div>
                            <div class="news-content">
                                <span><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></span>
                                <h4><?= $arItem['NAME'] ?></h4>
                                <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                            </div>
                        </div>
                    </div>
                </a>
            <? endforeach; ?>
        </div>
        <? if ($arResult['NAV_RESULT']->NavPageCount > 1): ?>
            <div class="news-more">
                <div class="content">
                    <a class="read-more ajax__more-link" href="javascript:void(0);"
                       data-all-page="<?= $arResult['NAV_RESULT']->NavPageCount ?>"
                       data-pagen="<?= $arResult['NAV_RESULT']->NavNum ?>"
                       data-page="<?= $arResult['NAV_RESULT']->PAGEN ?>">
                        <span class="read-more__label"><?= GetMessage('SHOW_MORE') ?></span>
                        <div class="read-more__icon">
                            <svg class="arrow" width="18" height="18">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#plus"></use>
                            </svg>
                        </div>
                    </a>
                </div>
            </div>
        <? endif; ?>
    </div>
</div>
