<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__); ?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
    <head>
        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/head.php', [], ['SHOW_BORDER' => false]) ?>
        <? $APPLICATION->ShowHead(); ?>
    </head>

    <body class="loading" data-temp="index">
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
    <div class="layer">
        <div class="layer__inner">
            <div class="layer__logo">
                <div class="layer__logo-img" style="background-image: url(<?= SITE_STYLE_PATH ?>/img/general/logo.svg)"></div>
            </div>
        </div>
    </div>
    <div class="main-wrapper">
        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/header.php', [], ['SHOW_BORDER' => false]) ?>
        <div class="container">
