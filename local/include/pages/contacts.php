<div class="contacts">
    <div class="contacts-map">
        <img src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_CONTACTS_MAP")) ?>" alt="">
    </div>
    <div class="contacts-wrap">
        <div class="contacts__line">
            <a class="contacts__more wow animate__animated animate__fadeInLeft" href="/trading-houses/"
               data-wow-delay="1.2s">
                    <span class="contacts__more-icon">
                        <svg class="arrow" width="14" height="14">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                        </svg>
                    </span>
                <div class="contacts__more-line">
                    <span class="contacts__more-label"><?= GetMessage('TRADING_HOUSES') ?></span>
                    <h4 data-hint-title="<?= GetMessage('CONTACTS_TRADING_HOUSE') ?>"><?= GetMessage('CONTACTS_TRADING_HOUSE') ?></h4>
                </div>
            </a>
            <div class="contacts-info wow animate__animated animate__fadeInLeft">
                <h4><?= GetMessage('MOSCOW_OFFICE') ?></h4>
                <a class="contacts-info__big-link"
                   href="tel:<?= preg_replace('/[^\d+]+/', '', \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE")) ?>"><?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_PHONE"); ?></a>
                <ul class="contacts__list contacts__list_base">
                    <li>
                        <div class="contacts__list-icon">
                            <svg class="icon" width="18" height="14">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#email"></use>
                            </svg>
                        </div>
                        <a href="mailto:<?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL"); ?>"><?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_EMAIL"); ?></a>
                    </li>
                    <li>
                        <div class="contacts__list-icon contacts__list-icon_fill">
                            <svg class="icon" width="17" height="21">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#label"></use>
                            </svg>
                        </div>
                        <?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_ADDRESS") ?>
                    </li>
                </ul>
                <a class="target-link target-link_white" href="#"
                   data-toggle="modal"
                   data-target="#modalContacts" data-wow-delay="1.2s">
                    <span class="target-link__label"><?= GetMessage('CALL_ME') ?></span>
                    <div class="target-link__icon">
                        <svg class="arrow" width="15" height="15">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                        </svg>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>