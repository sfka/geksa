<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="megamenu__list-inner">
        <? foreach ($arResult as $arItem):
            if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue; ?>
            <div class="megamenu__list-item megamenu__list-item_big">
                <div class="megamenu__list-brand">
                    <? if ($arItem['PARAMS']['LOGO']): ?>
                        <img src="<?= CFile::GetPath($arItem['PARAMS']['LOGO']) ?>" alt="<?= $arItem["TEXT"] ?>"/>
                    <? endif; ?>
                </div>
                <a class="megamenu__list-link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
            </div>
        <? endforeach ?>
    </div>
<? endif ?>