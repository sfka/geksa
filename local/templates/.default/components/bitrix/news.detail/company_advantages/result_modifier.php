<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

if (CModule::IncludeModule('highloadblock')) {
    foreach ($arResult['PROPERTIES'] as $key => &$property) {
        if ($property["PROPERTY_TYPE"] == "S" && $property["USER_TYPE_SETTINGS"]["TABLE_NAME"]) {
            $hldata = array_pop(HL\HighloadBlockTable::getList(['filter' => ['TABLE_NAME' => $property["USER_TYPE_SETTINGS"]["TABLE_NAME"]]])->fetchAll());
            $entityClass = HL\HighloadBlockTable::compileEntity($hldata)->getDataClass();
            $res = $entityClass::getList(['select' => ['*'], 'order' => ['ID' => 'ASC'], 'filter' => ['UF_XML_ID' => $property["VALUE"]]])->fetchAll();
            if (is_array($res) && !empty($res)) {
                $arResult['ADVANTAGES'] = $res;
            }
        }
    }
}