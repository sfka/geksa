<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="chapters" data-type="blue" data-view="main">
    <div class="chapters-info">
        <div class="chapters-info__container swiper-container chapters-slider">
            <div class="chapters-info__inner swiper-wrapper">
                <? $index = 0; ?>
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                    ?>
                    <div class="chapters-info__item swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <? if ($index > 0): ?>
                            <div class="chapters-info__label">
                                <span class="chapters-info__label-chapter"><?= GetMessage('CHAPTER') ?> <?= $index ?></span>
                                <h4><?= $arItem['NAME'] ?></h4>
                            </div>
                        <? endif; ?>
                        <div class="chapters-info__basis">
                            <? if ($arItem['PROPERTIES']['SVG']['VALUE']): ?>
                                <div class="chapters-info__illustration">
                                    <picture>
                                        <source media="(max-width:767px)"
                                                data-srcset="<?= CFile::GetPath($arItem['PROPERTIES']['SVG_MOBILE']['VALUE']) ?>">
                                        <img class="lazy"
                                             data-src="<?= CFile::GetPath($arItem['PROPERTIES']['SVG']['VALUE']) ?>"
                                             alt="<?= $arItem['NAME'] ?>">
                                    </picture>
                                </div>
                            <? endif; ?>
                            <div class="chapters-info__line">
                                <? if ($arItem['PROPERTIES']['NULL']['VALUE'] == "Y"): ?>
                                    <h1><?= $arItem['PROPERTIES']['ADD_NAME']['~VALUE']['TEXT'] ?></h1>
                                <? elseif ($arItem['PROPERTIES']['NULL']['VALUE'] != "Y" && !empty($arItem['PROPERTIES']['ADD_NAME']['~VALUE']['TEXT'])): ?>
                                    <div class="heading heading_level-1"><?= $arItem['PROPERTIES']['ADD_NAME']['~VALUE']['TEXT'] ?></div>
                                <? endif; ?>
                            </div>
                            <? if ($arItem['PROPERTIES']['LINK']['VALUE']): ?>
                                <a class="chapters-info__more chapters-info__more_main" href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>">
                                    <span class="chapters-info__more-icon">
                                        <svg class="icon__arrow-up-right" width="14" height="14">
                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                        </svg>
                                    </span>
                                    <div class="chapters-info__more-line">
                                        <span class="chapters-info__more-label"><?= $arItem['PROPERTIES']['LINK']['DESCRIPTION'] ?></span>
                                        <? if ($arItem['PROPERTIES']['LINK_TEXT']['VALUE']): ?>
                                            <h4><?= $arItem['PROPERTIES']['LINK_TEXT']['VALUE'] ?></h4>
                                        <? endif; ?>
                                    </div>
                                </a>
                            <? endif; ?>
                            <? if ($arItem['PROPERTIES']['SHOW_LINK_MARKS']['VALUE'] == 'Y'): ?>
                                <div class="chapters-info__personage">
                                    <div class="chapters-info__personage-row">
                                        <? $indexMark = 0; ?>
                                        <? foreach ($arResult['MARKS'] as $arMark): ?>
                                            <? $indexMark++; ?>
                                            <div class="chapters-info__personage-column">
                                                <div class="chapters-info__personage-item"
                                                     data-index="<?= $indexMark ?>">
                                                    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arMark['PROPERTIES']['SVG_MAN']['VALUE'])) ?>
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                                <div class="chapters-info__type">
                                    <div class="chapters-info__type-row">
                                        <? $indexMark = 0; ?>
                                        <? foreach ($arResult['MARKS'] as $arMark): ?>
                                            <? $indexMark++; ?>
                                            <div class="chapters-info__type-column">
                                                <a href="<?= $arMark['DETAIL_PAGE_URL'] ?>" class="chapters-info__type-item" data-index="<?= $indexMark ?>">
                                                    <div class="chapters-info__type-brand">
                                                        <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arMark['PROPERTIES']['LOGO_BLACK']['VALUE'])) ?>
                                                    </div>
                                                    <span class="chapters-info__type-icon">
                                                        <svg class="arrow" width="14" height="14">
                                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                                        </svg>
                                                    </span>
                                                </a>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                    <? $index++; ?>
                <? endforeach; ?>
            </div>
        </div>
    </div>
    <div class="chapters-steps">
        <? $count = 0 ?>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
            ?>
            <? if ($arItem['PROPERTIES']['NULL']['VALUE'] == "Y") continue; ?>
            <? $count++ ?>
            <div class="chapters-steps__item" data-index="<?= $count ?>"
                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="chapters-steps__item-column">
                    <div class="chapters-steps__item-line">
                        <span class="chapters-steps__item-count"><?= $count ?></span>
                    </div>
                    <div class="chapters-steps__item-line">
                        <span class="chapters-steps__item-chapter"><?= GetMessage('CHAPTER') ?> <?= $count ?></span>
                        <h4><?= $arItem['NAME'] ?></h4>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>