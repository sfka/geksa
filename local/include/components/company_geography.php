<? $arItems = IL\Catalog::getElementList($arParams['IBLOCK_ID']) ?>
<div class="geography-map__inner">
    <? $index = 0; ?>
    <? foreach ($arItems as $arItem): ?>
        <? $index++ ?>
        <div class="geography-map__label <?= ($index == 1 ? 'geography-map__label_active' : false) ?>"
             style="<?= $arItem['PROPERTIES']['STYLES']['VALUE'] ?>">
            <span class="geography-map__label-circle"></span>
            <div class="geography-map__info">
                <span><?= $arItem['PROPERTIES']['ADD_NAME']['VALUE'] ?></span>
                <h4><?= $arItem['NAME'] ?></h4>
                <p><?= GetMessage('SERVES') ?>: <?= $arItem['PROPERTIES']['SERVES']['~VALUE']['TEXT'] ?></p>
            </div>
        </div>
    <? endforeach; ?>
    <img src="<?= $arParams['MAP'] ?>"
         alt="<?= $arParams['NAME'] ?>">
</div>