<? /** @var $block array */ ?>
<div class="history__scroll">
    <? $index = 0; ?>
    <? foreach ($block['items'] as $arItem): ?>
        <? $index++ ?>
        <div class="history-group">
            <div class="history-date">
                <div class="history-date__item">
                    <span class="history-date__item-number"><?= $arItem['year'] ?></span>
                    <div class="history-date__item-line">
                        <span></span><span></span>
                    </div>
                </div>
            </div>
            <div class="history-info">
                <div class="max-width">
                    <? foreach ($arItem['phone'] as $event): ?>
                        <? if ($event != ""): ?>
                            <div class="history-info__item"><?= $event ?></div>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>