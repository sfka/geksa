<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="megamenu__bottom-inner">
        <div class="megamenu__bottom-item">
            <?
            foreach ($arResult as $arItem):
                if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                    continue;
                ?>
                <a class="megamenu__bottom-link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?>
                    <svg class="icon__arrow-up-right" width="8" height="8">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                    </svg>
                </a>
            <? endforeach ?>
        </div>
    </div>
<? endif ?>
