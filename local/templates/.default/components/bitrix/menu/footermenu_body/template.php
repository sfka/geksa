<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="footer__row footer__row_wrap">
        <? foreach ($arResult as $arItem): ?>
            <? if ($arItem['IS_PARENT']): ?>
                <div class="footer__item footer__item_accordion">
                    <div class="footer__list">
                        <a class="footer__list-link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?>
                            <svg class="icon__nav-menu_arrow" width="7" height="4">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#nav-menu_arrow"></use>
                            </svg>
                        </a>
                        <ul class="footer__sublist">
                            <? foreach ($arItem['ITEMS'] as $arSubItem): ?>
                                <li class="footer__sublist-item">
                                    <a class="footer__sublist-link"
                                       href="<?= $arSubItem['LINK'] ?>"><?= $arSubItem['TEXT'] ?></a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>
            <? endif; ?>
        <? endforeach; ?>
        <? if ($arResult['NO_PARENT']): ?>
            <div class="footer__item">
                <div class="footer__list">
                    <? foreach ($arResult['NO_PARENT'] as $arParItem): ?>
                        <a class="footer__list-link"
                           href="<?= $arParItem['LINK'] ?>"><?= $arParItem['TEXT'] ?></a>
                    <? endforeach; ?>
                </div>
            </div>
        <? endif; ?>
    </div>
<? endif; ?>
