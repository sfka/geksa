<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? $pageInfo = IL\Catalog::getElementList(ILBOCK_PAGES_ID, ['ID' => $arParams['ADD_PAGE_INFO']]); ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/header/inner_bg.php', ['ELEMENT' => $pageInfo[0]], ['SHOW_BORDER' => false]) ?>
<div id="target-container">
    <? foreach ($arResult['SECTIONS'] as $arSection): ?>
        <div class="section section_not-full certificates" id="<?= $arSection['CODE'] ?>">
            <div class="section-wrap">
                <div class="certificates-wrap">
                    <div class="certificates-label">
                        <div class="certificates-label__inner" data-sticky-container>
                            <div class="certificates-label__sticky sticky" data-sticky-class="is-sticky">
                                <div class="certificates-label__name"><?= $arSection['NAME'] ?></div>
                                <? if ($arSection['UF_LOGO']): ?>
                                    <div class="certificates-label__img">
                                        <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arSection['UF_LOGO'])) ?>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="certificates-info">
                        <div class="certificates-list" id="section_<?= $arSection['ID'] ?>">
                            <? foreach ($arSection['ITEMS'] as $arItem): ?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                                ?>
                                <a class="certificates-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                                   href="<?= CFile::GetPath($arItem['PROPERTIES']['FILE_PDF']['VALUE']) ?>"
                                   target="_blank">
                                    <div class="certificates-img">
                                        <div class="certificates-img__bg">
                                            <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                                                 alt="<?= $arItem['NAME'] ?>">
                                        </div>
                                        <span class="certificates-img__more">
											<svg class="icon__plus" width="18" height="18">
												<use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#plus"></use>
											</svg>
										</span>
                                    </div>
                                    <div class="certificates-content">
                                        <h4><?= $arItem['NAME'] ?></h4>
                                        <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                                    </div>
                                </a>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>