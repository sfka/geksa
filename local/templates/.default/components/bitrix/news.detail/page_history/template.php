<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="cover cover_interior">
    <? if ($arResult['PROPERTIES']['VIDEO_MAIN']['VALUE']): ?>
        <div class="cover__bg">
            <video autoplay="true" loop="true" muted="true" pip="false"
                   poster="<?= CFile::GetPath($arResult['PROPERTIES']['BG_MAIN']['VALUE']) ?>"
                   playsinline>
                <source src="<?= $arResult['PROPERTIES']['VIDEO_MAIN']['VALUE']['path'] ?>" type="video/mp4">
            </video>
        </div>
    <? elseif ($arResult['PROPERTIES']['BG_MAIN']['VALUE']): ?>
        <div class="cover__bg"
             style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['BG_MAIN']['VALUE']) ?>)"></div>
    <? endif; ?>
    <div class="cover-wrap">
        <div class="cover-content">
            <div class="cover-row">
                <div class="cover-column">
                    <h1><?= $arResult['PROPERTIES']['NAME_MAIN']['~VALUE']['TEXT'] ?></h1>
                    <p><?= $arResult['PROPERTIES']['TEXT_MAIN']['~VALUE']['TEXT'] ?></p>
                </div>
                <div class="cover-column">
                    <div class="scroll-bottom" data-href="#target-history">
                        <span class="scroll-bottom__label"><?= GetMessage('MOVE_DOWN') ?></span>
                        <div class="scroll-bottom__icon">
                            <svg class="icon__arrow-bottom" width="20" height="20">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-bottom"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="history" id="target-history">
    <? $APPLICATION->IncludeComponent(
        "sprint.editor:blocks",
        'content',
        [
            "ELEMENT_ID" => $arResult["ID"],
            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
            "PROPERTY_CODE" => 'CONTENT',
        ],
        $component,
        [
            "HIDE_ICONS" => "Y",
        ]
    ); ?>
</div>