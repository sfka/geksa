sprint_editor.registerBlock('my_slider', function ($, $el, data) {

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        data.items = [];
        $('.sp-izi-contacts-container .department').each(function () {
            var item = {};
            item['year'] = $(this).find('.contacts__department').val();
            item['phone'] = {};
            $(this).find('.department__contacts_row_phone .contacts__phone').each(function (index, value) {
                item['phone'][index] = {};
                item['phone'][index] = $(this).val();
            });
            data.items.push(item);
        });
        return data;
    };

    this.afterRender = function () {
        if (data.items) {
            $.each(data.items, function (index, item) {
                addRow(item);
            });
        }

        $el.on('click', '.sp-izi-add-contacts-row', function (e) {
            addRow({
                year: '',
            });
        });
        $el.on('click', '.department__contacts_add', function (e) {
            var type = $(this).attr('data-type');
            switch (type) {
                case '_phone':
                    var newRow = '<input placeholder="Событие" class="contacts__phone" value="">';
                    break;

            }
            $(this).closest('.department__contacts').find('.department__contacts_row' + type).append(newRow);
        });
    };

    function addRow(rowData) {
        $contactsRowPhone = '';
        if (typeof rowData.phone == 'object') {
            $.each(rowData.phone, function (index, value) {
                $contactsRowPhone +=
                    '<input placeholder="Событие" class="contacts__phone" value="' + value + '">';
            });
        }
        if(typeof rowData.phone != 'object') {
            $contactsRowPhone =
                '<div class="department__contacts_row_phone">\
                    <input placeholder="Событие" class="contacts__phone" value="">\
                </div>';
        }
        $rowHtml =
            '<div class="department">\
                <input placeholder="Год" class="contacts__department" value="' + rowData['year'] + '">\
                <div class="department__contacts">\
                    <input class="department__contacts_add" data-type="_phone" type="button" value="Добавить событие">\
                    <div class="department__contacts_row_phone">' + $contactsRowPhone + '</div>\
                </div>\
            </div>';
        $('.sp-izi-contacts-container').append($rowHtml);
    }
});
