<div class="information-info">
    <? if ($arParams['ELEMENT']['PROPERTIES']['SVG']['VALUE']): ?>
        <? if (CSite::InDir('/trading-houses/')): ?>
            <div class="information-info__bg information-info__bg_land">
                <img src="<?= \CFile::GetPath($arParams['ELEMENT']['PROPERTIES']['SVG']['VALUE']); ?>" alt="">
            </div>
        <? else: ?>
            <div class="information-info__bg information-info__bg_figure"
                 style="background-image: url(<?= \CFile::GetPath($arParams['ELEMENT']['PROPERTIES']['SVG']['VALUE']); ?>)">
            </div>
        <? endif; ?>
    <? endif; ?>
    <? $otherName = ($_SERVER['REAL_FILE_PATH'] == '/company/vacancies/index.php'); ?>
    <div class="information-info__line <?= ($otherName ? 'information-info_inner' : false) ?>">
        <h1><?= ($otherName ? $arParams['OTHER_NAME'] : $arParams['ELEMENT']['NAME']) ?></h1>
        <? if ($arParams['ELEMENT']['PREVIEW_TEXT']): ?>
            <p><?= $arParams['ELEMENT']['PREVIEW_TEXT'] ?></p>
        <? endif; ?>
    </div>
    <div class="information-info__line">
        <p><?= $arParams['ELEMENT']['PROPERTIES']['PREVIEW_TEXT2']['~VALUE']['TEXT'] ?></p>
        <? if ($arParams['ELEMENT']['PROPERTIES']['LINK']['VALUE']): ?>
            <a class="target-link target-link_white" href="<?= $arParams['ELEMENT']['PROPERTIES']['LINK']['VALUE'] ?>">
                <span class="target-link__label"><?= $arParams['ELEMENT']['PROPERTIES']['LINK']['DESCRIPTION'] ?></span>
                <div class="target-link__icon">
                    <svg class="arrow" width="15" height="15">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                    </svg>
                </div>
            </a>
        <? endif; ?>
    </div>
</div>