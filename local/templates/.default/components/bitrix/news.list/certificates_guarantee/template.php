<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container_interior">
    <div class="section section_padding certificates section_not-full">
        <div class="section-wrap">
            <h1 class="heading heading_level-1 heading_color heading_max-width"><?= $arResult['NAME'] ?></h1>
            <div class="certificates-wrap">
                <div class="certificates-label">
                    <div class="certificates-figure">
                        <div class="figure rellax"
                             style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/certificates/layer-1.svg)"
                             data-rellax-speed="1"></div>
                        <div class="figure rellax"
                             style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/certificates/layer-2.svg)"
                             data-rellax-speed="2"></div>
                    </div>
                </div>
                <div class="certificates-info">
                    <div class="certificates-list">
                        <? foreach ($arResult['ITEMS'] as $arItem): ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                            ?>
                            <a class="certificates-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                               href="<?= CFile::GetPath($arItem['PROPERTIES']['FILE_PDF']['VALUE']) ?>" target="_blank">
                                <div class="certificates-img">
                                    <div class="certificates-img__bg">
                                        <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                                             alt="<?= $arItem['NAME'] ?>">
                                    </div>
                                    <span class="certificates-img__more">
                                    <svg class="icon__plus" width="18" height="18">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#plus"></use>
                                    </svg>
                                </span>
                                </div>
                                <div class="certificates-content">
                                    <h4><?= $arItem['NAME'] ?></h4>
                                    <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                                </div>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>