<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/telegramm.php");
$name = "telegramm";
$title = GetMessage("BOOKMARK_HANDLER_TELEGRAMM");
$icon_url_template = '
<a target="_blank" class="socials-icons__item telegram" href="https://telegram.me/share/url?url=#PAGE_URL#&text=#PAGE_TITLE_UTF_ENCODED#" title="' . $title . '">
    <svg class="icon" width="18" height="15">
        <use xlink:href="' . SITE_STYLE_PATH . '/img/general/svg-symbols.svg#telegram"></use>
    </svg>
</a>';
?>