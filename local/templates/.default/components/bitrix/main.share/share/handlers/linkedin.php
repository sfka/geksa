<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/linkedin.php");
$name = "linkedin";
$title = GetMessage("BOOKMARK_HANDLER_LINKEDIN");
$icon_url_template = '
<a target="_blank" class="socials-icons__item linkedin" 
    href="https://www.linkedin.com/sharing/share-offsite/?url={#PAGE_URL#}&title={#PAGE_TITLE_UTF_ENCODED#}" title="' . $title . '">
    <svg class="icon" width="18" height="15">
        <use xlink:href="' . SITE_STYLE_PATH . '/img/general/svg-symbols.svg#linkedin"></use>
    </svg>
</a>';
?>
