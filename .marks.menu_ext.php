<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

if (CModule::IncludeModule("iblock")) {

    $IBLOCK_ID = ILBOCK_MARKS_ID;

    $arOrder = ["SORT" => "ASC"];
    $arSelect = ["ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_LOGO", "PROPERTY_NAME_MENU"];
    $arFilter = ["IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y"];
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $aMenuLinksExt[] = [
            $arFields['PROPERTY_NAME_MENU_VALUE'] ? $arFields['PROPERTY_NAME_MENU_VALUE'] : $arFields['NAME'],
            $arFields['DETAIL_PAGE_URL'],
            [],
            [
                "LOGO" => $arFields['PROPERTY_LOGO_VALUE'],
            ],
            "",
        ];
    }
}

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);