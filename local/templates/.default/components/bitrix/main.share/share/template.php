<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if ($arResult["PAGE_URL"]) : ?>
    <div class="socials">
        <span class="socials__label"><?= GetMessage('SHARE') ?>:</span>
        <div class="socials-icons">
            <? if (is_array($arResult["BOOKMARKS"]) && count($arResult["BOOKMARKS"]) > 0) : ?>
                <? foreach (array_reverse($arResult["BOOKMARKS"]) as $name => $arBookmark): ?>
                    <?= $arBookmark["ICON"] ?>
                <? endforeach; ?>
            <? endif; ?>
        </div>
    </div>
<? endif; ?>
