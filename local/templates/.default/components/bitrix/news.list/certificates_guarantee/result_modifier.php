<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arResult['SECTIONS'] = [];
foreach ($arResult['ITEMS'] as $arItem) {
    if (!array_key_exists($arItem['IBLOCK_SECTION_ID'], $arResult['SECTIONS'])) {
        $sectionInfo = IL\Catalog::getSectionsByID($arParams['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'], ['UF_LOGO']);
        $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']] = $sectionInfo[0];
    }
    $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['ITEMS'][] = $arItem;
}
usort($arResult['SECTIONS'], function($a, $b){
    return ($a['SORT'] - $b['SORT']);
});