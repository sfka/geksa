<? /** @var $block array */ ?><?
$image = Sprint\Editor\Blocks\Image::getImage(
    $block, [
        'width' => 1024,
        'height' => 768,
        'exact' => 0,
        //'jpg_quality' => 75
    ]
);
?><? if ($image): ?>
    <figure>
        <img src="<?= $image['SRC'] ?>" alt="<?= $image['DESCRIPTION'] ?>">
        <? if ($image['DESCRIPTION']): ?>
            <figcaption><?= $image['DESCRIPTION'] ?></figcaption>
        <? endif; ?>
    </figure>
<? endif; ?>
