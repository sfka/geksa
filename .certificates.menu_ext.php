<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

if (CModule::IncludeModule("iblock")) {

    $IBLOCK_ID = ILBOCK_CERTIFICATES_ID;

    $arFilter = ['IBLOCK_ID' => $IBLOCK_ID];
    $rsSect = CIBlockSection::GetList(['SORT' => 'ASC'], $arFilter, false, ['ID', 'NAME', 'SECTION_PAGE_URL', 'UF_*']);
    while ($arSect = $rsSect->GetNext()) {
        $aMenuLinksExt[] = [
            $arSect['NAME'],
            $arSect['CODE'],
            [],
            [
                "HREF_ID" => $arSect['ID'],
                "LOGO" => $arSect['UF_LOGO'],
                "NOT_MENU" => $arSect['UF_NOT_MENU'],
            ],
            "",
        ];
    }
}

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);