<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section section_white section_padding advantages">
    <div class="section__bg"
         style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['BG_ADV']['VALUE']) ?>)"></div>
    <div class="section-wrap">
        <div class="content">
            <div class="section__line">
                <div class="content-center">
                    <div class="max-width">
                        <div class="heading heading_level-1 heading_medium"><?= $arResult['PROPERTIES']['NAME_ADV']['~VALUE']['TEXT'] ?></div>
                        <p><?= $arResult['PROPERTIES']['TEXT_ADV']['~VALUE']['TEXT'] ?></p>
                    </div>
                </div>
            </div>
            <div class="section__line">
                <div class="advantages-list">
                    <div class="advantages-list__row">
                        <? foreach ($arResult['ADVANTAGES'] as $arAdv): ?>
                            <div class="advantages-list__item">
                                <div class="advantages-list__item-icon">
                                    <img src="<?= CFile::GetPath($arAdv['UF_FILE']) ?>" alt="<?= $arAdv['UF_NAME'] ?>">
                                </div>
                                <h4><?= $arAdv['UF_NAME'] ?></h4>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>