<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
foreach ($arResult['SECTIONS'] as $key => $arSection) {
    $res = CIBlockElement::GetList(
        ['SORT' => 'ASC'],
        ['IBLOCK_ID' => $arSection['IBLOCK_ID'], 'IBLOCK_SECTION_ID' => $arSection['ID']],
        false,
        ["nPageSize" => 3],
        ['ID', 'NAME', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'ACTIVE_FROM', 'PREVIEW_TEXT']);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();
        $arFields['PROPERTIES'] = $arProps;
        $arResult['SECTIONS'][$key]['ITEMS'][] = $arFields;
    }
}