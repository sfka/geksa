<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/ok.php");
$name = "ok";
$title = GetMessage(" ");
$icon_url_template = "
<a
	href=\"https://connect.ok.ru/offer?url=#PAGE_URL_ENCODED#title==#PAGE_TITLE_UTF_ENCODED#\"
	onclick=\"window.open(this.href,'','toolbar=0,status=0,width=626,height=436');return false;\"
	target=\"_blank\"
	class=\"item-socials ok\"
	title=\"".$title."\"
>
<svg class=\"icon\" width=\"9\" height=\"16\">
        <use xlink:href=\"" . SITE_STYLE_PATH . "/img/general/svg-symbols.svg#ok\"></use>
</svg>
</a>\n";
$sort = 30;
?>