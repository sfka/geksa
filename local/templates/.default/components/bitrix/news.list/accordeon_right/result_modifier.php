<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arResult['SECTIONS'] = [];
foreach ($arResult['ITEMS'] as $arItem) {
    $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['NAME'] = IL\Catalog::getSectionNameByID($arParams['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID']);
    $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['ITEMS'][] = $arItem;
    if ($arItem['ID'] == $_SESSION['CITY']) {
        $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['CHOOSEN'] = 'Y';
    }
}