<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arResult['CHAPTER_STEPS'] = [
    [
        'NAME' => $arResult['NAME'],
        'ICO' => CFile::GetPath($arResult['PROPERTIES']['NAME_ICO']['VALUE']),
        'ADD_TEXT' => $arResult['PROPERTIES']['ADD_NAME'],
        'ADD_TEXT2' => $arResult['PROPERTIES']['ADD_NAME_TEXT'],
        'IMAGE' => CFile::GetPath($arResult['PROPERTIES']['ADD_NAME_IMAGE']['VALUE']),
        'IMAGE_MOB' => CFile::GetPath($arResult['PROPERTIES']['ADD_NAME_IMAGE_MOB']['VALUE']),
    ],
    [
        'NAME' => GetMessage('MARK_NUMBERS'),
        'ICO' => CFile::GetPath($arResult['PROPERTIES']['NUMBERS_ICO']['VALUE']),
        'INFO' => $arResult['PROPERTIES']['NUMBERS'],
        'PREVIEW_TEXT' => $arResult['PROPERTIES']['NUMBERS_TEXT'],
        'IMAGE' => CFile::GetPath($arResult['PROPERTIES']['NUMBERS_IMAGE']['VALUE']),
        'IMAGE_MOB' => CFile::GetPath($arResult['PROPERTIES']['NUMBERS_IMAGE_MOB']['VALUE']),
    ],
    [
        'NAME' => GetMessage('MARK_PRODUCTS'),
        'ICO' => CFile::GetPath($arResult['PROPERTIES']['PRODUCTS_ICO']['VALUE']),
        'INFO' => $arResult['PROPERTIES']['PRODUCTS'],
        'PREVIEW_TEXT' => $arResult['PROPERTIES']['PRODUCTS_TEXT'],
        'IMAGE' => CFile::GetPath($arResult['PROPERTIES']['PRODUCTS_IMAGE']['VALUE']),
        'IMAGE_MOB' => CFile::GetPath($arResult['PROPERTIES']['PRODUCTS_IMAGE_MOB']['VALUE']),
    ],
    [
        'NAME' => GetMessage('MARK_CONTACTS'),
        'ICO' => CFile::GetPath($arResult['PROPERTIES']['LINK_ICO']['VALUE']),
        'INFO' => $arResult['PROPERTIES']['LINK'],
        'PREVIEW_TEXT' => $arResult['PROPERTIES']['LINK_TEXT'],
        'IMAGE' => CFile::GetPath($arResult['PROPERTIES']['LINK_IMAGE']['VALUE']),
        'IMAGE_MOB' => CFile::GetPath($arResult['PROPERTIES']['LINK_IMAGE_MOB']['VALUE']),
        'IMAGE_SITE' => CFile::GetPath($arResult['PROPERTIES']['LINK_IMAGE_SITE']['VALUE']),
    ],
];