<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="cover">
    <? if ($arResult['PROPERTIES']['VIDEO_MAIN']['VALUE']): ?>
        <div class="cover__bg">
            <video autoplay="true" loop="true" muted="true" pip="false"
                   poster="<?= CFile::GetPath($arResult['PROPERTIES']['BG_MAIN']['VALUE']) ?>"
                   playsinline>
                <source src="<?= $arResult['PROPERTIES']['VIDEO_MAIN']['VALUE']['path'] ?>" type="video/mp4">
            </video>
        </div>
    <? elseif ($arResult['PROPERTIES']['BG_MAIN']['VALUE']): ?>
        <div class="cover__bg"
             style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['BG_MAIN']['VALUE']) ?>)"></div>
    <? endif; ?>
    <div class="cover-wrap">
        <div class="cover-content">
            <div class="cover-row">
                <div class="cover-column">
                    <h1><?= $arResult['PROPERTIES']['NAME_MAIN']['~VALUE']['TEXT'] ?></h1>
                    <p><?= $arResult['PROPERTIES']['TEXT_MAIN']['~VALUE']['TEXT'] ?></p>
                </div>
                <div class="cover-column">
                    <div class="scroll-bottom" data-href="#target-information">
                        <span class="scroll-bottom__label"><?= GetMessage('MOVE_DOWN') ?></span>
                        <div class="scroll-bottom__icon">
                            <svg class="icon__arrow-bottom" width="20" height="20">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-bottom"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="information information_text" id="target-information">
    <div class="information-info">
        <div class="information-info__bg information-info__bg_figure">
            <div class="figure rellax"
                 style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/information/layer-1.svg)"
                 data-rellax-speed="1"></div>
            <div class="figure rellax"
                 style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/information/layer-2.svg)"
                 data-rellax-speed="2"></div>
            <div class="figure figure_360"
                 style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/information/figure-360.svg)"></div>
        </div>
    </div>
    <div class="information-scroll">
        <div class="information-scroll__inner">
            <div class="information-accordion">
                <div class="heading heading_level-1 heading_medium"><?= $arResult['PROPERTIES']['TEXT_PROCESS']['~VALUE']['TEXT'] ?></div>
                <? $index = 0; ?>
                <? foreach ($arResult['PROPERTIES']['PROCESS']['~VALUE'] as $key => $value): ?>
                    <? $index++; ?>
                    <div class="accordion">
                        <div class="accordion-title">
                            <div class="accordion-title__column">
                                <h4><?= $arResult['PROPERTIES']['PROCESS']['DESCRIPTION'][$key] ?></h4>
                            </div>
                            <span class="accordion-title__icon">
                                <svg class="icon" width="18" height="18">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#accordion-plus"></use>
                                </svg>
                            </span>
                        </div>
                        <div class="accordion-content">
                            <p><?= $value['TEXT'] ?></p>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>
<div class="section section_padding advantages">
    <div class="section__bg"
         style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['BG_EFFECT']['VALUE']) ?>)"></div>
    <div class="section-wrap">
        <div class="advantages-wrap advantages-wrap_center">
            <div class="advantages-content section_white">
                <div class="content">
                    <div class="content-center">
                        <div class="max-width">
                            <div class="heading heading_level-1"><?= $arResult['PROPERTIES']['NAME_EFFECT']['~VALUE']['TEXT'] ?></div>
                            <p><?= $arResult['PROPERTIES']['TEXT_EFFECT']['~VALUE']['TEXT'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <? if ($arResult['PROPERTIES']['NUMBERS_EFFECT']['VALUE']): ?>
                <div class="advantages-info">
                    <div class="advantages-counts">
                        <? $index = 0; ?>
                        <? foreach ($arResult['PROPERTIES']['NUMBERS_EFFECT']['VALUE'] as $key => $val): ?>
                            <? $index++; ?>
                            <div class="advantages-counts__item <?= ($index % 2 ? false : 'relax') ?>" <?= ($index % 2 ? false : 'data-rellax-speed="0.9"') ?>>
                                <div class="advantages-counts__item-icon">
                                    <img src="<?= CFile::GetPath($val) ?>"
                                         alt="">
                                </div>
                                <h4><?= $arResult['PROPERTIES']['LINK_EFFECT']['VALUE'][$key] ?></h4>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>
<div class="section section_padding advantages">
    <? if ($arResult['PROPERTIES']['BG_VIDEO_STANDARTS']['VALUE']): ?>
        <div class="section__bg">
            <video autoplay="true" loop="true" muted="true" pip="false"
                   poster="<?= CFile::GetPath($arResult['PROPERTIES']['BG_STANDARTS']['VALUE']) ?>"
                   playsinline>
                <source src="<?= $arResult['PROPERTIES']['BG_VIDEO_STANDARTS']['VALUE']['path'] ?>" type="video/mp4">
            </video>
        </div>
    <? else: ?>
        <div class="section__bg"
             style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['BG_STANDARTS']['VALUE']) ?>)"></div>
    <? endif; ?>
    <div class="section-wrap">
        <div class="advantages-wrap advantages-wrap_center">
            <div class="advantages-content section_white">
                <div class="content">
                    <div class="max-width"></div>
                </div>
            </div>
            <div class="advantages-info">
                <div class="advantages-links">
                    <? $index = 0; ?>
                    <? foreach ($arResult['PROPERTIES']['LINKS']['VALUE'] as $key => $val): ?>
                        <? $index++; ?>
                        <a class="advantages-links__item <?= ($index % 2 ? 'advantages-links__item_bg' : false) ?>"
                            <?= (substr($val, 0, 1) === "#") ? 'href="javascript:void(0);" data-toggle="modal" data-target="' . $val . '"' : 'href="' . $val . '"' ?>>
                            <span class="advantages-links__item-icon">
                                <svg class="arrow" width="14" height="14">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                </svg>
                            </span>
                            <h4><?= $arResult['PROPERTIES']['LINKS']['DESCRIPTION'][$key] ?></h4>
                        </a>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>