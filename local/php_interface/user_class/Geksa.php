<?

namespace IL;

use \Bitrix\Main\Loader;
use CMain;

/**
 * $APPLICATION
 * Класс для работы с проектом
 * Class Geksa
 * @package IL
 */
class Geksa
{

    /**
     * Устанавливает ShowViewContent для шапки
     * @param $elementID
     */
    public static function setHeaderInfo($elementID)
    {
        $pageInfo = Catalog::getElementList(ILBOCK_PAGES_ID, ['ID' => $elementID]);
        global $APPLICATION;
        if ($pageInfo[0]['PREVIEW_TEXT']) {
            $html = '<p>' . $pageInfo[0]['PREVIEW_TEXT'] . '</p>';
            $APPLICATION->AddViewContent('LONG_NAME', $html);
        }
        if ($pageInfo[0]['PROPERTIES']['PREVIEW_TEXT2']['VALUE']) {
            $html = '<p>' . $pageInfo[0]['PROPERTIES']['PREVIEW_TEXT2']['~VALUE']['TEXT'] . '</p>';
            $APPLICATION->AddViewContent('LONG_NAME_BOTTOM', $html);
        }
        if ($pageInfo[0]['PROPERTIES']['LINK']['VALUE']) {
            $html = '
            ';
            $APPLICATION->AddViewContent('LINK', $html);
        }
        if ($pageInfo[0]['PREVIEW_PICTURE']) {
            $APPLICATION->AddViewContent('PAGE_BG', \CFile::GetPath($pageInfo[0]['PREVIEW_PICTURE']));
        } elseif ($pageInfo[0]['PROPERTIES']['SVG']['VALUE']) {
            $APPLICATION->AddViewContent('PAGE_BG', \CFile::GetPath($pageInfo[0]['PROPERTIES']['SVG']['VALUE']));
        }
    }

}