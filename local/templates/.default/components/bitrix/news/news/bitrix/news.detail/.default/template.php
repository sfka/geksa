<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container_interior">
    <div class="section section_padding section_not-full article">
        <div class="section-wrap">
            <article data-sticky-container>
                <a class="all-link sticky" href="<?= $arParams['IBLOCK_URL'] ?>" data-sticky-class="is-sticky">
                    <div class="all-link__icon">
                        <svg class="icon__back-link" width="20" height="20">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#back-link"></use>
                        </svg>
                    </div>
                    <span class="all-link__label"><?= GetMessage('MOVE_NEWS') ?></span>
                </a>
                <div class="content">
                    <img src="<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arResult['NAME'] ?>">
                    <time datetime="<?= $arResult['DISPLAY_ACTIVE_FROM'] ?>"><?= $arResult['DISPLAY_ACTIVE_FROM'] ?></time>
                    <? $APPLICATION->IncludeComponent(
                        "sprint.editor:blocks",
                        'content',
                        [
                            "ELEMENT_ID" => $arResult["ID"],
                            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                            "PROPERTY_CODE" => 'CONTENT',
                        ],
                        $component,
                        [
                            "HIDE_ICONS" => "Y",
                        ]
                    ); ?>
                </div>
            </article>
            <div class="article-info">
                <div class="article-info__wrap">
                    <div class="content">
                        <div class="article-info__inner">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.share",
                                "share",
                                [
                                    "HANDLERS" => [0 => "telegramm", 1 => "twitter", 2 => "facebook", 3 => "linkedin"],
                                    "HIDE" => "N",
                                    "PAGE_TITLE" => "",
                                    "PAGE_URL" => "",
                                    "SHORTEN_URL_KEY" => "",
                                    "SHORTEN_URL_LOGIN" => "",
                                    "PAGE_URL" => $APPLICATION->GetCurPage(),
                                    "PAGE_TITLE" => $arResult['NAME'],
                                ]
                            ); ?>
                            <? foreach ($arResult['DISPLAY_PROPERTIES']['MEDIA']['LINK_ELEMENT_VALUE'] as $link): ?>
                                <a class="target-link" href="<?= $link['DETAIL_PAGE_URL'] ?>">
                                    <span class="target-link__label"><?= GetMessage('MOVE_ALL_PHOTOS_NEWS') ?></span>
                                    <div class="target-link__icon">
                                        <svg class="icon__arrow-up-right" width="15" height="15">
                                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                        </svg>
                                    </div>
                                </a>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>