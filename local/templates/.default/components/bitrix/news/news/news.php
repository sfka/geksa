<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? $APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "press-center",
    [
        "ADD_SECTIONS_CHAIN" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COUNT_ELEMENTS" => "N",
        "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
        "FILTER_NAME" => "",
        "IBLOCK_ID" => "5",
        "IBLOCK_TYPE" => "presscenter",
        "SECTION_CODE" => "",
        "SECTION_FIELDS" => [
            0 => "",
            1 => "",
        ],
        "SECTION_ID" => "",
        "SECTION_URL" => "/press-center/#SECTION_CODE#/",
        "SECTION_USER_FIELDS" => [
            0 => "UF_ADD_NAME",
            1 => "UF_SHOW_MORE",
            2 => "",
        ],
        "SHOW_PARENT_NAME" => "Y",
        "TOP_DEPTH" => "2",
        "VIEW_MODE" => "LINE",
        "COMPONENT_TEMPLATE" => "press-center",
        'ADD_PAGE_INFO' => $arParams['ADD_PAGE_INFO'],
    ],
    false
); ?>
<div class="banner">
    <div class="content">
        <div class="banner-content">
            <div class="banner-row">
                <div class="banner-column">
                    <div class="banner__title"><?= GetMessage('MEDIABANK_DESC') ?></div>
                </div>
                <div class="banner-column">
                    <a class="banner-info" href="/press-center/media-bank/">
                        <div class="banner-info__img">
                            <img src="<?= SITE_STYLE_PATH ?>/img/content/banner/bg.svg" alt="">
                        </div>
                        <div class="banner-info__more">
                            <svg class="icon__arrow-up-right" width="14" height="14">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                            </svg>
                        </div>
                        <h4><?= GetMessage('MEDIABANK_TITLE') ?></h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
