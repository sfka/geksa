<div class="modal fade" id="modalVacancies" tabindex="-1" role="dialog" aria-labelledby="modalVacanciesLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-start">
            <div class="modal-content">
                <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                    <svg class="icon" width="26" height="25">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close-modal"></use>
                    </svg>
                </button>
                <div class="modal-title">
                    <span><?= GetMessage('VACANCY_RESPONSE') ?></span>
                    <h4 class="js__modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-form">
                        <form class="default-form ajax__form form__file" action="<?= SITE_AJAX_PATH ?>/form_vacancy.php"
                              method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="type_file" value="pdf, doc, docx, xml, xls">
                            <input type="hidden" name="vacancy" value="" id="vacancy_id">
                            <input type="hidden" name="city"
                                   value="<?= ($_SESSION['CITY'] ? IL\Catalog::getElementNameByID(ILBOCK_CITIES_ID, $_SESSION['CITY']) : '') ?>"
                                   id="vacancy_city">
                            <div class="form-group">
                                <div class="form-inputs">
                                    <div class="form-input">
                                        <input class="form-control" placeholder="" id="vacinput2" name="name"/>
                                        <label class="form-input__label"
                                               for="vacinput2"><?= GetMessage('FORM_NAME') ?></label>
                                    </div>
                                    <div class="form-input">
                                        <input class="form-control phone-mask" type="tel" placeholder="" id="vacinput6"
                                               name="phone"/>
                                        <label class="form-input__label"
                                               for="vacinput6"><?= GetMessage('FORM_PHONE') ?></label>
                                    </div>
                                    <div class="form-input">
                                        <input class="form-control" type="email" placeholder="" id="vacinput5"
                                               name="email"/>
                                        <label class="form-input__label"
                                               for="vacinput5"><?= GetMessage('FORM_EMAIL') ?></label>
                                    </div>
                                </div>
                                <div class="form-file__wrap">
                                    <label class="form-file" for="vacformFile">
                                        <input type="hidden"/>
                                        <input type="file" name="file" id="vacformFile"/>
                                        <div class="form-file__icon">
                                            <div class="form-file__icon-show">
                                                <svg class="icon" width="16" height="18">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#file"></use>
                                                </svg>
                                            </div>
                                            <div class="form-file__icon-hide">
                                                <div class="form-file__close">
                                                    <svg class="icon" width="10" height="10">
                                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#file-close"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-file__text"
                                             data-text="<?= GetMessage('ATTACH_RESUME') ?>"><?= GetMessage('ATTACH_RESUME') ?></div>
                                    </label>
                                    <div class="form-file__hint"
                                         data-hint-text="<?= GetMessage('FILE_ATTACHMENT') ?>"><?= GetMessage('FILE_ATTACH_DESC') ?></div>
                                </div>
                                <div class="form-btn">
                                    <button class="btn-next">
                                        <span class="btn-next__label"><?= GetMessage('RESPOND') ?></span>
                                        <div class="btn-next__icon">
                                            <svg class="arrow" width="15" height="15">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                            </svg>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-success">
            <div class="modal-success__text">
                <img src="<?= SITE_STYLE_PATH ?>/img/content/modals/done.svg"
                     alt=""/><span><?= GetMessage('FORM_SUCCESS') ?></span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalContacts" tabindex="-1" role="dialog" aria-labelledby="modalContactsLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-start">
            <div class="modal-content">
                <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                    <svg class="icon" width="26" height="25">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close-modal"></use>
                    </svg>
                </button>
                <div class="modal-title">
                    <h4><?= GetMessage('FEEDBACK') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-form">
                        <form class="default-form ajax__form" action="<?= SITE_AJAX_PATH ?>/form_contacts.php"
                              method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="city"
                                   value="<?= ($_SESSION['CITY'] ? IL\Catalog::getElementNameByID(ILBOCK_CITIES_ID, $_SESSION['CITY']) : '') ?>"
                                   id="contact_city">
                            <div class="form-group">
                                <div class="form-inputs">
                                    <div class="form-input">
                                        <input class="form-control" placeholder="" id="contactinput1" name="name"/>
                                        <label class="form-input__label"
                                               for="contactinput1"><?= GetMessage('FORM_NAME') ?></label>
                                    </div>
                                    <div class="form-input">
                                        <input class="form-control phone-mask" type="tel" placeholder=""
                                               id="contactinput2"
                                               name="phone"/>
                                        <label class="form-input__label"
                                               for="contactinput2"><?= GetMessage('FORM_PHONE') ?></label>
                                    </div>
                                    <? $questions = \Bitrix\Main\Config\Option::get("askaron.settings", "UF_CONTACTS_THEMES"); ?>
                                    <div class="form-input">
                                        <select class="form-control" name="theme">
                                            <option><?= GetMessage('QUESTION_THEME') ?></option>
                                            <? foreach ($questions as $question): ?>
                                                <option><?= $question ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-btn">
                                    <button class="btn-next">
                                        <span class="btn-next__label"><?= GetMessage('SEND') ?></span>
                                        <div class="btn-next__icon">
                                            <svg class="arrow" width="15" height="15">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                            </svg>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-success">
            <div class="modal-success__text">
                <img src="<?= SITE_STYLE_PATH ?>/img/content/modals/done.svg"
                     alt=""/><span><?= GetMessage('FORM_SUCCESS') ?></span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTradingHouses" tabindex="-1" role="dialog" aria-labelledby="modalTradingHousesLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                <svg class="icon" width="26" height="25">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close-modal"></use>
                </svg>
            </button>
            <div class="ajax__modal-content"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalCooperation" tabindex="-1" role="dialog" aria-labelledby="modalCooperationLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-start">
            <div class="modal-content">
                <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                    <svg class="icon" width="26" height="25">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close-modal"></use>
                    </svg>
                </button>
                <div class="modal-title">
                    <h4><?= GetMessage('COOPERATION_ORDER') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-form">
                        <form class="default-form ajax__form" action="<?= SITE_AJAX_PATH ?>/form_cooperation.php"
                              method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="city"
                                   value="<?= ($_SESSION['CITY'] ? IL\Catalog::getElementNameByID(ILBOCK_CITIES_ID, $_SESSION['CITY']) : '') ?>"
                                   id="coop_city">
                            <div class="form-group">
                                <div class="form-inputs">
                                    <div class="form-input">
                                        <input class="form-control" placeholder="" id="coopinput1" name="name"/>
                                        <label class="form-input__label"
                                               for="coopinput1"><?= GetMessage('FORM_NAME') ?></label>
                                    </div>
                                    <div class="form-input">
                                        <input class="form-control phone-mask" type="tel" placeholder="" id="input2"
                                               name="phone"/>
                                        <label class="form-input__label"
                                               for="input2"><?= GetMessage('FORM_PHONE') ?></label>
                                    </div>
                                    <div class="form-input">
                                        <input class="form-control" placeholder="" id="coopinput3" name="company"/>
                                        <label class="form-input__label"
                                               for="coopinput3"><?= GetMessage('FORM_COMPANY') ?></label>
                                    </div>
                                </div>
                                <div class="form-btn">
                                    <button class="btn-next"><span
                                                class="btn-next__label"><?= GetMessage('SEND') ?></span>
                                        <div class="btn-next__icon">
                                            <svg class="arrow" width="15" height="15">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                            </svg>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-success">
            <div class="modal-success__text">
                <img src="<?= SITE_STYLE_PATH ?>/img/content/modals/done.svg"
                     alt=""/><span><?= GetMessage('FORM_SUCCESS') ?></span>
            </div>
        </div>
    </div>
</div>

<div class="modal modal_wide fade" id="modalCity" tabindex="-1" role="dialog" aria-labelledby="modalCityLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                <svg class="icon" width="26" height="25">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close-modal"></use>
                </svg>
            </button>
            <div class="modal-title">
                <h4><?= GetMessage('CHOOSE_YOUR_CITY') ?></h4>
            </div>
            <div class="modal-body">
                <div class="modal-city">
                    <div class="modal-city__input">
                        <div class="form-input">
                            <input class="form-control js__input_city" placeholder="" id="inputCity" name="city"/>
                            <label class="form-input__label" for="inputCity"><?= GetMessage('INPUT_CITY') ?></label>
                        </div>
                    </div>
                    <div class="modal-city__list">
                        <? $arCities = IL\Catalog::getElementList(ILBOCK_CITIES_ID, [], ['ID', 'NAME']); ?>
                        <div class="modal-city__list-scroll">
                            <ul>
                                <? foreach ($arCities as $arCity): ?>
                                    <li class="ajax__choose_city" data-id="<?= $arCity['ID'] ?>"
                                        data-city="<?= $arCity['NAME'] ?>"><?= $arCity['NAME'] ?></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>