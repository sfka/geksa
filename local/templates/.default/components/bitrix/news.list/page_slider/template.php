<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="chapters chapters_interior" data-type="blue">
    <div class="chapters-info">
        <div class="chapters-info__container swiper-container chapters-slider">
            <div class="chapters-info__inner swiper-wrapper">
                <? $index = 0; ?>
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <? $index++; ?>
                    <div class="chapters-info__item swiper-slide">
                        <div class="chapters-info__basis">
                            <div class="chapters-info__bg">
                                <? if ($arItem['PROPERTIES']['BG_VIDEO']['VALUE']): ?>
                                    <video autoplay="true" loop="true" muted="true" pip="false"
                                           poster="<?= CFile::GetPath($arItem['PROPERTIES']['BG']['VALUE']) ?>"
                                           playsinline>
                                        <source src="<?= $arItem['PROPERTIES']['BG_VIDEO']['VALUE']['path'] ?>"
                                                type="video/mp4">
                                    </video>
                                <? else: ?>
                                    <img src="<?= CFile::GetPath($arItem['PROPERTIES']['BG']['VALUE']) ?>"
                                         alt="<?= $arItem['NAME'] ?>">
                                <? endif; ?>
                            </div>
                            <? if ($index == 1): ?>
                                <div class="chapters-info__line">
                                    <? if ($arItem['PROPERTIES']['ADD_NAME']['~VALUE']['TEXT']): ?>
                                        <h1><?= $arItem['PROPERTIES']['ADD_NAME']['~VALUE']['TEXT'] ?></h1>
                                    <? endif; ?>
                                    <? if ($arItem['PROPERTIES']['ADD_TEXT']['~VALUE']['TEXT']): ?>
                                        <p><?= $arItem['PROPERTIES']['ADD_TEXT']['~VALUE']['TEXT'] ?></p>
                                    <? endif; ?>
                                </div>
                            <? else: ?>
                                <div class="chapters-info__count">
                                    <div class="chapters-info__count-inner">
                                        <span><?= $arItem['PROPERTIES']['ADD_NAME']['~VALUE']['TEXT'] ?></span>
                                        <p><?= $arItem['PROPERTIES']['ADD_TEXT']['~VALUE']['TEXT'] ?></p>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="scroll-bottom" data-href="#target-container">
            <span class="scroll-bottom__label"><?= GetMessage('MOVE_DOWN') ?></span>
            <div class="scroll-bottom__icon">
                <svg class="icon__arrow-bottom" width="20" height="20">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-bottom"></use>
                </svg>
            </div>
        </div>
    </div>
    <div class="chapters-steps">
        <? $count = 0; ?>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
            ?>
            <div class="chapters-steps__item <?= ($count == 0 ? 'chapters-steps__item_active' : false) ?>"
                 data-index="<?= $count ?>"
                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="chapters-steps__item-column">
                    <? if ($arItem['PROPERTIES']['ICO_SVG']['VALUE']): ?>
                        <div class="chapters-steps__item-line">
                            <div class="chapters-steps__item-icon">
                                <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arItem['PROPERTIES']['ICO_SVG']['VALUE'])) ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="chapters-steps__item-line">
                        <h4><?= $arItem['NAME'] ?></h4>
                    </div>
                </div>
            </div>
            <? $count++; ?>
        <? endforeach; ?>
    </div>
</div>