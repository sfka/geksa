<?
if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    $obForm = new \IL\FormVacancy(IBLOCK_FORM_VACANCY);
    $arFields = $_POST;
    if (!empty($_FILES['file']['name'][0])) {
        $arFields['FILES'] = $_FILES;
    }
    $arResult = $obForm->add($arFields);
    if ($arResult === true) {
        echo json_encode([
            'status' => 'ok',
            'title' => 'Спасибо, вы успешно записались!',
            'message' => 'Администратор салона «Качество жизни» свяжется с вами в ближайшее время.',
        ]);
    } else {
        echo json_encode([
            'status' => 'error',
            'message' => $arResult
        ]);
    }
}