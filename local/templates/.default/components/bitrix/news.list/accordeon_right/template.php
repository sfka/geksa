<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="information <?= ($arParams['NO_PADDING_TOP'] ? false : 'information_interior') ?>">
    <? $pageInfo = IL\Catalog::getElementList(ILBOCK_PAGES_ID, ['ID' => $arParams['ADD_PAGE_INFO']]); ?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/header/inner_bg_column.php', ['ELEMENT' => $pageInfo[0]], ['SHOW_BORDER' => false]) ?>
    <div class="information-scroll">
        <div class="information-scroll__inner">
            <div class="information-accordion">
                <div class="heading heading_level-1 heading_color"><?= ($arParams['OTHER_NAME'] ? $arParams['OTHER_NAME'] : $arResult['NAME']) ?></div>
                <? foreach ($arResult['SECTIONS'] as $arSection): ?>
                    <? if (count($arSection['ITEMS']) > 0): ?>
                        <div class="accordion <?= ($arSection['CHOOSEN'] == 'Y' ? 'accordion_open' : false) ?>">
                            <div class="accordion-title">
                                <div class="accordion-title__column">
                                    <h4><?= $arSection['NAME'] ?></h4>
                                    <? if ($arParams['SHOW_ITEMS_IN_SECTION'] == "Y"): ?>
                                        <div class="accordion-title__desc">
                                            <? $counter = 0; ?>
                                            <? foreach ($arSection['ITEMS'] as $arItem): ?>
                                                <? $counter++; ?>
                                                <?= $arItem['NAME'] . ($counter != count($arSection['ITEMS']) ? ', ' : false); ?>
                                            <? endforeach; ?>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <span class="accordion-title__icon">
                                <svg class="icon" width="18" height="18">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#accordion-plus"></use>
                                </svg>
                            </span>
                            </div>
                            <div class="accordion-content">
                                <div class="accordion-list">
                                    <? foreach ($arSection['ITEMS'] as $arItem): ?>
                                        <?
                                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                                        ?>
                                        <a class="accordion-list__item <?= ($arParams['LINK_MODAL'] == "Y" ? 'ajax__modal-update' : false) ?>"
                                            <? if ($arParams['LINK_MODAL'] == "Y"): ?>
                                                href="#"
                                                data-toggle="modal"
                                                data-target="#modalTradingHouses"
                                                data-element="<?= $arItem['ID'] ?>"
                                            <? else: ?>
                                                href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                                            <? endif; ?>
                                           id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                            <div class="accordion-list__item-title"><?= $arItem['NAME'] ?></div>
                                            <div class="accordion-list__item-desc">
                                                <? if ($arItem['PROPERTIES']['SERVES']['~VALUE']['TEXT']): ?>
                                                    <?= $arItem['PROPERTIES']['SERVES']['~VALUE']['TEXT'] ?>
                                                <? else: ?>
                                                    <? if ($arItem['PROPERTIES']['WORK_MODE']['~VALUE']['TEXT']): ?>
                                                        <span><?= $arItem['PROPERTIES']['WORK_MODE']['~VALUE']['TEXT'] ?></span>
                                                    <? endif; ?>
                                                    <? if ($arItem['PROPERTIES']['PREVIEW_TEXT']['~VALUE']['TEXT']): ?>
                                                        <span><?= $arItem['PROPERTIES']['PREVIEW_TEXT']['~VALUE']['TEXT'] ?></span>
                                                    <? endif; ?>
                                                <? endif; ?>
                                            </div>
                                        </a>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
    </div>
    <? if ($arParams['AJAX_ELEMENT_ID']): ?>
        <div class="ajax__content">
            <? $elementID = $arParams['AJAX_ELEMENT_ID'];
            $index = array_search($elementID, array_column($arResult['ITEMS'], 'ID'));
            $arItem = $arResult['ITEMS'][$index]; ?>
            <div class="modal-title">
                <span><?= $arItem['PROPERTIES']['ADD_NAME']['VALUE'] ?></span>
                <h3><?= $arItem['NAME'] ?></h3>
            </div>
            <div class="modal-body">
                <div class="modal-contacts">
                    <span><?= $arItem['PROPERTIES']['SERVES']['~VALUE']['TEXT'] ?></span>
                    <ul class="modal-contacts__list modal-contacts__list_base">
                        <? if ($arItem['PROPERTIES']['PHONE']['VALUE']): ?>
                            <li>
                                <div class="modal-contacts__icon">
                                    <svg class="icon" width="17" height="17">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#phone"></use>
                                    </svg>
                                </div>
                                <ul class="modal-contacts__list">
                                    <? foreach ($arItem['PROPERTIES']['PHONE']['VALUE'] as $phone): ?>
                                        <li>
                                            <a href="tel:<?= preg_replace('/[^\d+]+/', '', $phone); ?>"><?= $phone ?></a>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </li>
                        <? endif; ?>
                        <? if ($arItem['PROPERTIES']['EMAIL']['VALUE']): ?>
                            <li>
                                <div class="modal-contacts__icon">
                                    <svg class="icon" width="18" height="14">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#email"></use>
                                    </svg>
                                </div>
                                <? foreach ($arItem['PROPERTIES']['EMAIL']['VALUE'] as $email): ?>
                                    <a href="mailto:<?= $email ?>"><?= $email ?></a>
                                <? endforeach; ?>
                            </li>
                        <? endif; ?>
                        <? if ($arItem['PROPERTIES']['ADDRESS']['~VALUE']): ?>
                            <li>
                                <div class="modal-contacts__icon modal-contacts__icon_fill">
                                    <svg class="icon" width="17" height="21">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#label"></use>
                                    </svg>
                                </div>
                                <?= $arItem['PROPERTIES']['ADDRESS']['~VALUE'] ?>
                            </li>
                        <? endif; ?>
                        <? if ($arItem['PROPERTIES']['WORK_MODE']['~VALUE']): ?>
                            <li>
                                <div class="modal-contacts__icon">
                                    <svg class="icon" width="17" height="17">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#time"></use>
                                    </svg>
                                </div>
                                <?= $arItem['PROPERTIES']['WORK_MODE']['~VALUE'] ?>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
                <? if ($arItem['PROPERTIES']['ADDRESS']['~VALUE']): ?>
                    <div class="modal-btn">
                        <a class="target-link"
                           href="https://yandex.ru/maps/?text={<?= $arItem['PROPERTIES']['ADDRESS']['~VALUE'] ?>}"
                           target="_blank">
                            <span class="target-link__label"><?= GetMessage('SHOW_IN_MAP') ?></span>
                            <div class="target-link__icon">
                                <svg class="arrow" width="15" height="15">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                                </svg>
                            </div>
                        </a>
                    </div>
                <? endif; ?>
            </div>
        </div>
    <? endif; ?>
</div>
