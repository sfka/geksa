<?

namespace IL;

/**
 * Класс настроек
 * Class Settings
 * @package IL
 */
class Settings {

    const CACHE_TIME = 3600;
    const REVIEWS_IBLOCK_ID = 2;
    const PROGRAM_FORM_ID = 16;

}