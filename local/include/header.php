<header class="header">
    <div class="header__inner">
        <a class="header__logo-link" href="<?= SITE_DIR ?>">
            <div class="header__logo">
                <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_LOGO"))); ?>
            </div>
        </a>
        <div class="header__main">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top_menu",
                [
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "2",
                    "MENU_CACHE_GET_VARS" => [
                    ],
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                    "COMPONENT_TEMPLATE" => "top_menu",
                ],
                false
            ); ?>
            <div class="header__info">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top_menu_standarts",
                    [
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => [
                        ],
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "certificates",
                        "USE_EXT" => "Y",
                        "COMPONENT_TEMPLATE" => "top_menu_standarts",
                    ],
                    false
                ); ?>
                <div class="city-selection">
                    <div class="city-selection__inner" data-toggle="modal" data-target="#modalCity">
                        <div class="city-selection__icon">
                            <svg class="icon__city-selection" width="19" height="23">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#city-selection"></use>
                            </svg>
                        </div>
                        <span class="city-selection__name"><?= ($_SESSION['CITY'] ? IL\Catalog::getElementNameByID(ILBOCK_CITIES_ID, $_SESSION['CITY']) : GetMessage('CHOOSE_CITY')) ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="burger">
            <div class="burger__line"></div>
            <div class="burger__line"></div>
        </div>
    </div>
    <? $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "left_menu",
        [
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "left",
            "DELAY" => "N",
            "MAX_LEVEL" => "1",
            "MENU_CACHE_GET_VARS" => [
            ],
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "left",
            "USE_EXT" => "Y",
            "COMPONENT_TEMPLATE" => "left_menu",
        ],
        false
    ); ?>
    <div class="megamenu">
        <div class="megamenu__wrap">
            <div class="megamenu__inner">
                <div class="megamenu__list megamenu__list_type">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "mega_marks_menu",
                        [
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => [
                            ],
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "marks",
                            "USE_EXT" => "Y",
                            "COMPONENT_TEMPLATE" => "mega_marks_menu",
                        ],
                        $component,
                        ['HIDE_ICONS' => 'Y']
                    ); ?>
                </div>
                <div class="megamenu__list megamenu__list_base">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "megamenu_body",
                        [
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "2",
                            "MENU_CACHE_GET_VARS" => [
                            ],
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "bottom_main",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "megamenu_body",
                        ],
                        false
                    ); ?>
                </div>
                <div class="megamenu__bottom">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "megamenu_bottom",
                        [
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => [
                            ],
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "bottom_footer",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "megamenu_bottom",
                        ],
                        false
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</header>