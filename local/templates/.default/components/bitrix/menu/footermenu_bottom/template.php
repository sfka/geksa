<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="footer__links">
        <?
        foreach ($arResult as $arItem):
            if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
            ?>
            <div class="footer__links-item">
                <a class="footer__links-link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?>
                    <svg class="arrow" width="8" height="8">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                    </svg>
                </a>
            </div>
        <? endforeach ?>
    </div>
<? endif ?>
