<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container_interior">
    <div class="section section_padding section_not-full article">
        <div class="section-wrap">
            <article data-sticky-container>
                <a class="all-link sticky" href="<?= $arParams['IBLOCK_URL'] ?>" data-sticky-class="is-sticky">
                    <div class="all-link__icon">
                        <svg class="icon__back-link" width="20" height="20">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#back-link"></use>
                        </svg>
                    </div>
                    <span class="all-link__label"><?= GetMessage('MOVE_MEDIA_BANK') ?></span>
                </a>
                <div class="content">
                    <img src="<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arResult['NAME'] ?>">
                    <time datetime="<?= $arResult['DISPLAY_ACTIVE_FROM'] ?>"><?= $arResult['DISPLAY_ACTIVE_FROM'] ?></time>
                    <h3><?= $arResult['NAME'] ?></h3>
                </div>
            </article>
            <? if ($arResult['PROPERTIES']['PHOTOS']['VALUE']): ?>
                <div class="article-media">
                    <div class="content">
                        <h4><?= GetMessage('PHOTO') ?></h4>
                        <div class="article-media__row article-media__row_photo">
                            <? foreach ($arResult['PROPERTIES']['PHOTOS']['VALUE'] as $photo): ?>
                                <? $photoSrc = CFile::GetPath($photo) ?>
                                <div class="article-media__column">
                                    <div class="article-media__item">
                                        <a class="photo" href="javascript:void(0);" data-fancybox="gallery"
                                           data-options="{&quot;src&quot; : &quot;<?= $photoSrc ?>&quot;}">
                                            <img class="lazy" data-src="<?= $photoSrc ?>"
                                                 alt="">
                                        </a>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            <? endif; ?>
            <? if ($arResult['PROPERTIES']['VIDEOS']['VALUE']): ?>
                <div class="article-media">
                    <div class="content">
                        <h4><?= GetMessage('VIDEO') ?></h4>
                        <div class="article-media__row article-media__row_video">
                            <? foreach ($arResult['PROPERTIES']['VIDEOS']['VALUE'] as $key => $video): ?>
                                <? if ($arResult['PROPERTIES']['VIDEOS']['DESCRIPTION'][$key]): ?>
                                    <? $photoSrc = CFile::GetPath($video) ?>
                                    <div class="article-media__column">
                                        <div class="article-media__item">
                                            <div class="video">
                                                <iframe data-type="iframe" width="560" height="315"
                                                        src="https://www.youtube.com/embed/<?= $arResult['PROPERTIES']['VIDEOS']['DESCRIPTION'][$key] ?>"
                                                        frameborder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowfullscreen></iframe>
                                                <div class="video-play">
                                                    <span class="video-play__icon"></span>
                                                    <img class="lazy"
                                                         data-src="<?= $photoSrc ?>"
                                                         alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>