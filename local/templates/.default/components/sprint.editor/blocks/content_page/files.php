<? /**
 * @var $block array
 * @var $this  SprintEditorBlocksComponent
 */ ?><?
?><? if (!empty($block['files'])): ?>
    <? foreach ($block['files'] as $item): ?>
        <p>
            <a href="<?= $item['file']['SRC'] ?>" download="<?= $item['file']['ORIGINAL_NAME'] ?>"
               title="<?= $item['desc'] ?>" target="_blank"><?= $item['desc'] ?>
                <sup>
                    <svg class="icon__arrow-up-right" width="8" height="8">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-up-right"></use>
                    </svg>
                </sup>
            </a>
        </p>
    <? endforeach; ?>
<? endif; ?>
