<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="standards">
        <? foreach ($arResult as $arItem): ?>
            <? if (!$arItem['PARAMS']['NOT_MENU']): ?>
                <a class="standards__item" href="/production/certificates/#<?= str_replace("/", "", $arItem['LINK']) ?>"
                    <?= (stripos($arItem['LINK'], 'http') === false ? false : 'target="_blank"') ?>>
                    <div class="standards__inner">
                        <? if (is_numeric($arItem['PARAMS']['LOGO'])): ?>
                            <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arItem['PARAMS']['LOGO'])) ?>
                        <? else: ?>
                            <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . $arItem['PARAMS']['LOGO']) ?>
                        <? endif; ?>
                    </div>
                    <div class="standards__hint">
                        <div class="standards__hint-inner"><?= $arItem['TEXT'] ?></div>
                    </div>
                </a>
            <? endif; ?>
        <? endforeach; ?>
    </div>
<? endif ?>
