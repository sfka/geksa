<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="cover cover_interior">
    <? if ($arResult['PROPERTIES']['VIDEO_MAIN']['VALUE']): ?>
        <div class="cover__bg">
            <video autoplay="true" loop="true" muted="true" pip="false"
                   poster="<?= CFile::GetPath($arResult['PROPERTIES']['BG_MAIN']['VALUE']) ?>"
                   playsinline>
                <source src="<?= $arResult['PROPERTIES']['VIDEO_MAIN']['VALUE']['path'] ?>" type="video/mp4">
            </video>
        </div>
    <? elseif ($arResult['PROPERTIES']['BG_MAIN']['VALUE']): ?>
        <div class="cover__bg"
             style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['BG_MAIN']['VALUE']) ?>)"></div>
    <? endif; ?>
    <div class="cover-wrap">
        <div class="cover-content">
            <div class="cover-row">
                <div class="cover-column">
                    <h1><?= $arResult['PROPERTIES']['NAME_MAIN']['~VALUE']['TEXT'] ?></h1>
                    <p><?= $arResult['PROPERTIES']['TEXT_MAIN']['~VALUE']['TEXT'] ?></p>
                </div>
                <div class="cover-column">
                    <div class="scroll-bottom" data-href="#target-mission">
                        <span class="scroll-bottom__label"><?= GetMessage('MOVE_DOWN') ?></span>
                        <div class="scroll-bottom__icon">
                            <svg class="icon__arrow-bottom" width="20" height="20">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-bottom"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section section_padding mission" id="target-mission">
    <div class="mission__figure">
        <div class="figure rellax" style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/mission/layer-2.svg)"
             data-rellax-speed="1"></div>
        <div class="figure rellax" style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/mission/layer-3.svg)"
             data-rellax-speed="3"></div>
        <div class="figure rellax" style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/mission/layer-4.svg)"
             data-rellax-speed="2"></div>
        <div class="figure rellax" style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/mission/layer-1.svg)"
             data-rellax-speed="-3"></div>
        <div class="figure figure_360"
             style="background-image: url(<?= SITE_STYLE_PATH ?>/img/content/mission/figure-360.svg)"></div>
    </div>
    <div class="section-wrap">
        <div class="content">
            <div class="heading heading_level-1 heading_medium heading_color"><?= $arResult['PROPERTIES']['NAME_MISSION']['~VALUE']['TEXT'] ?></div>
            <? if (!empty($arResult['PREVIEW_TEXT'])): ?>
                <div class="hide-text">
                    <div class="hide-text__inner">
                        <?= $arResult['PREVIEW_TEXT'] ?>
                    </div>
                    <div class="hide-text__button" data-open-text="Скрыть текст">Читать полностью</div>
                </div>
            <? endif; ?>
            <div class="mission-list">
                <? $index = 0; ?>
                <? foreach ($arResult['PROPERTIES']['MISSION']['~VALUE'] as $key => $value): ?>
                    <? $index++; ?>
                    <div class="mission-item rellax" data-rellax-speed="<?= ($index % 2 ? '-0.5' : '1.1') ?>">
                        <div class="mission-item__title">
                            <span class="mission-item__icon">
                                <svg class="icon" width="18" height="18">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#accordion-plus"></use>
                                </svg>
                            </span>
                            <h4><?= $arResult['PROPERTIES']['MISSION']['DESCRIPTION'][$key] ?></h4>
                        </div>
                        <div class="mission-item__content">
                            <p><?= $value['TEXT'] ?></p>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>
<div class="section section_padding leadership">
    <div class="section-wrap">
        <div class="leadership-wrap">
            <div class="leadership-img">
                <img src="<?= CFile::GetPath($arResult['PROPERTIES']['BG_LETTER']['VALUE']) ?>" alt="">
            </div>
            <div class="leadership-content">
                <div class="max-width">
                    <div class="heading heading_level-1 heading_medium"><?= $arResult['PROPERTIES']['NAME_LETTER']['~VALUE']['TEXT'] ?></div>
                    <div class="leadership__cite">
                        <p><?= $arResult['PROPERTIES']['TEXT_LETTER']['~VALUE']['TEXT'] ?></p>
                        <div class="leadership__cite-signature">
                            <p><strong><?= $arResult['PROPERTIES']['TEXT_LETTER_MAN']['VALUE'] ?></strong></p>
                            <p><strong><?= $arResult['PROPERTIES']['TEXT_LETTER_MAN']['DESCRIPTION'] ?></strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>