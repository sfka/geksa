<? /** @var $block array */
?>
<? foreach ($block['items'] as $item): ?>
    <? if ($item['number']): ?>
        <figure>
            <figcaption>
                <p><strong><?= $item['number'] ?></strong>
                </p>
                <p><?= $item['number_name'] ?></p>
                <ul>
                    <li>
                        <svg class="icon__phone" width="17" height="17">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#phone"></use>
                        </svg>
                        <? foreach ($item['phone'] as $phone): ?>
                            <div>
                                <? $mainPhone = explode('(доб', $phone)[0];
                                $addPhone = explode('(доб', $phone)[1]; ?>
                                <a href="tel:<?= preg_replace('/[^\d+]+/', '', $mainPhone); ?>"><?= $mainPhone ?></a>
                                <span><?= '(доб' . $addPhone ?></span>
                            </div>
                        <? endforeach; ?>
                    </li>
                    <li>
                        <? foreach ($item['email'] as $email): ?>
                            <svg class="icon__email" width="18" height="14">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#email"></use>
                            </svg>
                            <a href="mailto:<?= $email ?>"><?= $email ?></a>
                        <? endforeach; ?>
                    </li>
                </ul>
            </figcaption>
        </figure>
    <? endif; ?>
<? endforeach; ?>


