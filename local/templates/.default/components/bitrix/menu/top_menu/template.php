<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="header__main-nav">
        <nav class="nav-menu">
            <? foreach ($arResult as $arItem): ?>
                <? if ($arItem['PARAMS']['SHOW_POPUP_MENU'] != "Y"): ?>
                    <div class="nav-menu__item <?= $arItem['SELECTED'] ? 'nav-menu__item_active' : false ?>">
                        <a class="nav-menu__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </div>
                <? else: ?>
                    <div class="nav-menu__item nav-menu__item_drop nav-menu__item_marks">
                        <a class="nav-menu__link" href="javascript:void(0);"
                           data-hint-title="Продукция"><?= $arItem["TEXT"] ?>
                            <svg class="icon__nav-menu_arrow" width="7" height="4">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#nav-menu_arrow"></use>
                            </svg>
                        </a>
                        <ul class="nav-menu__dropdown">
                            <? foreach ($arItem['ITEMS'] as $arSubItem): ?>
                                <li class="nav-menu__dropdown-item">
                                    <a class="nav-menu__dropdown-link"
                                       href="<?= $arSubItem["LINK"] ?>">
                                        <span class="nav-menu__dropdown-title"><?= $arSubItem["TEXT"] ?></span>
                                        <? if (!empty($arSubItem['PARAMS']['NAME_TWO'])): ?>
                                            <span class="nav-menu__dropdown-desc"><?= $arSubItem['PARAMS']['NAME_TWO'] ?></span>
                                        <? endif; ?>
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                <? endif; ?>
            <? endforeach; ?>
        </nav>
    </div>
<? endif ?>
