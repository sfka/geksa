<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!empty($arResult)) {
    $arResult = IL\Utilities::getMultilevelArray($arResult);
}
foreach ($arResult as $key => $arItem) {
    if ($arItem['IS_PARENT'] === false) {
        $arResult['NO_PARENT'][] = $arResult[$key];
    }
}