<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
    <div class="nav-submenu nav-submenu_active">
        <div class="nav-submenu__inner">
            <ul class="nav-submenu__menu">
                <? foreach ($arResult as $arItem): ?>
                    <li class="nav-submenu__menu-item <?= ($arItem['SELECTED'] ? 'nav-submenu__menu-item_active' : false) ?>">
                        <a class="nav-submenu__menu-link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
<? endif ?>
