<?

namespace IL;

use \Bitrix\Main\Loader;

/**
 * Класс для работы с формой заявка на услугу
 * Class FormFeedback
 * @package IL
 */
class FormFeedback extends Iblock {

    /**
     * Добавляет элемент с данными из формы в ИБ
     * @param $arFields
     * @return bool|string
     */
    public function add($arFields) {
        if (!Loader::includeModule('iblock')) return false;

        $el = new \CIBlockElement;
        $arLoadProductArray = [
            "IBLOCK_ID" => $this->iblockID,
            "DATE_ACTIVE_FROM" => date('d.m.Y H:i:s'),
            "PROPERTY_VALUES" => [
                'PHONE' => $arFields['phone'],
                'NAME' => $arFields['name'],
                'SERVICE' => $arFields['service'],
            ],
            "NAME" => "Заявка от " . date('d.m.Y H:i:s'),
            "ACTIVE" => "N",
            "TYPE" => "html",
        ];
        $fieldText = '';
        foreach ($arLoadProductArray['PROPERTY_VALUES'] as $key => $values) {
            if ($values != '') {
                if ($key == 'SERVICE') {
                    $elemName = Catalog::getElementNameByID(Settings::SERVICES_IBLOCK_ID, $values);
                    $values = $elemName;
                }
                $resName = \CIBlockProperty::GetByID($key, $this->iblockID, false)->GetNext();
                $name = $resName['NAME'];
                $fieldText .= $name . ': ' . $values . '<br>';
            }
        }
        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arEventFields = [
                "FORM_NAME" => "Заявка на услугу",
                "TEXT" => $fieldText,
            ];
            \CEvent::Send("FORMS", SITE_ID, $arEventFields, 'N', 31);
            return true;
        } else {
            return $el->LAST_ERROR;
        }
    }

}